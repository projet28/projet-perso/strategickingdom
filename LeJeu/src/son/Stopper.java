package son;

public class Stopper extends Thread{
	private Sound musique;
    public Stopper (Sound musique) {
    	this.musique = musique;
    }

    @Override
    public void run() {
        // Initialisation
    	try {
			if(musique != null) musique.stop();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	}
}
