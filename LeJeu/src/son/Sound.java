package son;


import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import javazoom.jl.player.advanced.*;
        
        
        public class Sound {
        	 private boolean isPlaying = false;
             private AdvancedPlayer player = null;
             
                public Sound(String path) throws Exception {
                        InputStream in = (InputStream) new BufferedInputStream( new FileInputStream( new File(path)));
                        player = new AdvancedPlayer(in);
                }
                
                public void play() throws Exception {
                        if (player != null) {
                                isPlaying = true;
                                player.play();
                        }
                }
                
                public void stop() throws Exception {
                        if (player != null) {
                                isPlaying = false;
                                player.close();
                        }
                }
                
                public boolean isPlaying() {
                        return isPlaying;
                }
 
               
        }

