package fenetre;

import java.util.ArrayList;
import java.util.List;

import graphTimer.StdDraw;


public class Onglet {
	
	private List<String> onglet;
	private String type;

	public Onglet(){
		type = "build";
		onglet = new ArrayList<String>();
			onglet.add("ferme");
			onglet.add("mine");
			onglet.add("auberge");
			onglet.add("port");
			onglet.add("tour");
	}
	
	public List<String> getOnglet() {
		return onglet;
	}

	public void setOnglet(List<String> onglet) {
		this.onglet = onglet;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void affiche() {
		StdDraw.setPenRadius();
		StdDraw.setPenColor(StdDraw.BLACK);
		StdDraw.filledRectangle(0.918, 0.74, 0.065, 0.25);
		StdDraw.setPenColor(StdDraw.BOOK_BLUE);
		StdDraw.setPenRadius(0.008);
		StdDraw.rectangle(0.918, 0.74, 0.065, 0.25);
		StdDraw.line(0.98, 0.99, 0.85, 0.99);
		StdDraw.line(0.98, 0.99, 0.98, 0.49);
		StdDraw.line(0.85, 0.99, 0.85, 0.49);
		StdDraw.line(0.98, 0.69, 0.85, 0.69);
		StdDraw.line(0.98, 0.79, 0.85, 0.79);
		StdDraw.line(0.98, 0.59, 0.85, 0.59);
		StdDraw.line(0.98, 0.49, 0.85, 0.49);
		StdDraw.line(0.98, 0.89, 0.85, 0.89);
		
		double x = 0.92;
		double y = 0.94;
		for (String ong: onglet) {
		    if(ong != null) StdDraw.picture(x, y, "Image/"+ong+".png");
		    y = y - 0.1;
		}
	}
}
