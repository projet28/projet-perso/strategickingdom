package fenetre;

import java.util.ArrayList;
import java.util.List;

import environment.Entite;
import graphTimer.StdDraw;
import graphTimer.Timer;
import main.Main;

public class Ath {
	
	private List<String> message;
	private List<Timer> time;
	
	private Boolean voir = false;

	public Ath(){
		message = new ArrayList<String>();
		message.add("bonjeu");
		time = new ArrayList<Timer>();
		Timer t = new Timer(2000);
		time.add(t);
	}
	
	public void SetImage(String image) {
		Timer t = new Timer(2000);
		time.add(t);
		message.add(image);
		
	}
	
	public void voir(double x, double y) {
		if(Main.perso1.isTourJoueur()) {
			for(Entite ent: Main.perso1.getEntite()) {
				if(ent.getPos().getX() < x+0.05 && ent.getPos().getX() > x-0.05 
						&& ent.getPos().getY() < y+0.025 && ent.getPos().getY() > y-0.025) {
					
					if(ent.toString(1) != null && !voir) {
						StdDraw.text(0.1, 0.32, ent.toString(1));
						StdDraw.text(0.1, 0.28, ent.toString(2));
						voir = true;
					}
				}else {
					voir = false;
				}
			}
		}else {
			for(Entite ent: Main.perso2.getEntite()) {
				if(ent.getPos().getX() < x+0.05 && ent.getPos().getX() > x-0.05 
						&& ent.getPos().getY() < y+0.025 && ent.getPos().getY() > y-0.025) {
					
					if(ent.toString(1) != null && !voir) {
						StdDraw.text(0.1, 0.32, ent.toString(1));
						StdDraw.text(0.1, 0.28, ent.toString(2));
						voir = true;
					}
				}else {
					voir = false;
				}
			}
		}
	}
	
	public void affiche() {
		StdDraw.setPenRadius();
		StdDraw.setPenColor(StdDraw.BLACK);
		StdDraw.filledRectangle(0.1, 0.3, 0.1, 0.05);
		StdDraw.setPenColor(StdDraw.BOOK_BLUE);
		StdDraw.setPenRadius(0.008);
		StdDraw.rectangle(0.1, 0.3, 0.1, 0.05);
		StdDraw.setPenRadius();
		StdDraw.setPenColor(StdDraw.CYAN);
		
		StdDraw.setPenRadius();
		StdDraw.setPenColor(StdDraw.BLACK);
		StdDraw.filledRectangle(0.5, 0.97, 0.1, 0.025);
		StdDraw.setPenColor(StdDraw.BOOK_BLUE);
		StdDraw.setPenRadius(0.008);
		StdDraw.rectangle(0.5, 0.97, 0.1, 0.025);
		StdDraw.setPenRadius();
		StdDraw.setPenColor(StdDraw.CYAN);
		StdDraw.text(0.5, 0.97, "passer son tour");
		
		StdDraw.setPenColor(StdDraw.BLACK);
		StdDraw.filledRectangle(0.15, 0.97, 0.15, 0.02);
		StdDraw.setPenColor(StdDraw.BOOK_BLUE);
		StdDraw.setPenRadius(0.008);
		StdDraw.rectangle(0.15, 0.97, 0.15, 0.02);
		StdDraw.setPenRadius();
		StdDraw.setPenColor(StdDraw.CYAN);
		StdDraw.text(0.15, 0.97, Main.perso1.toString());
		
		StdDraw.setPenColor(StdDraw.BLACK);
		StdDraw.filledRectangle(0.15, 0.93, 0.15, 0.02);
		StdDraw.setPenColor(StdDraw.BOOK_BLUE);
		StdDraw.setPenRadius(0.008);
		StdDraw.rectangle(0.15, 0.93, 0.15, 0.02);
		StdDraw.setPenRadius();
		StdDraw.setPenColor(StdDraw.CYAN);
		StdDraw.text(0.15, 0.93, Main.perso2.toString());
		
		double y=0;
		int i =0;
		int supp =-1;
		for(String mess : message) {
			if(time.get(i).hasFinished()) {
				supp = i;
			}
			StdDraw.setPenColor(StdDraw.BLACK);
			StdDraw.filledRectangle(0.45, 0.5+y, 0.35, 0.03);
			StdDraw.setPenColor(StdDraw.BOOK_BLUE);
			StdDraw.setPenRadius(0.008);
			StdDraw.rectangle(0.45, 0.5+y, 0.35, 0.03);
			StdDraw.setPenRadius();
			StdDraw.picture(0.5, 0.5+y, "Image/"+mess+".png");
			i++;
			y = y+ 0.1;
		}
		if(supp != -1) {
			message.remove(supp);
			time.remove(supp);
		}
	}
}
