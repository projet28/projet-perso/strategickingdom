package fenetre;

import java.util.ArrayList;
import java.util.List;

import environment.Boat;
import environment.Case;
import environment.Caserne;
import environment.Champ;
import environment.Entite;
import environment.Ferme;
import environment.Fermier;
import environment.Guerrier;
import environment.Habitant;
import environment.Marin;
import environment.Mine;
import environment.Mineur;
import environment.Porte;
import environment.Soldat;
import environment.Tour;
import graphTimer.Position;
import graphTimer.StdDraw;
import graphTimer.Timer;
import graphTimer.Triplet;
import main.Main;

public class Interaction {

	private Timer touche = new Timer(500);

	private boolean buildFerme = false;
	private boolean buildMine = false;
	private boolean buildAuberge = false;
	private boolean buildPort = false;
	private boolean buildTour = false;
	private boolean buildChamp = false;
	private boolean buildBoat1 = false;

	private Soldat leSoldat = null;
	private Entite mouvSoldat = null;
	private Boat mouvBoat = null;

	private Triplet batiment = null;

	public void bougeMap(double x, double y) {
		Position p = new Position(x, y);
		Main.grille.setP(p);
	}

	public boolean isBuildFerme() {
		return buildFerme;
	}

	public void setBuildFerme(boolean buildFerme) {
		this.buildFerme = buildFerme;
	}

	public boolean isBuildMine() {
		return buildMine;
	}

	public void setBuildMine(boolean buildMine) {
		this.buildMine = buildMine;
	}

	public boolean isBuildAuberge() {
		return buildAuberge;
	}

	public void setBuildAuberge(boolean buildAuberge) {
		this.buildAuberge = buildAuberge;
	}

	public boolean isBuildPort() {
		return buildPort;
	}

	public void setBuildPort(boolean buildPort) {
		this.buildPort = buildPort;
	}

	public boolean isBuildTour() {
		return buildTour;
	}

	public void setBuildTour(boolean buildTour) {
		this.buildTour = buildTour;
	}

	public boolean isBuildChamp() {
		return buildChamp;
	}

	public void setBuildChamp(boolean buildChamp) {
		this.buildChamp = buildChamp;
	}

	public boolean isBuildBoat1() {
		return buildBoat1;
	}

	public void setBuildBoat1(boolean buildBoat1) {
		this.buildBoat1 = buildBoat1;
	}

	public Soldat getLeSoldat() {
		return leSoldat;
	}

	public void setLeSoldat(Soldat leSoldat) {
		this.leSoldat = leSoldat;
	}

	public Entite getMouvSoldat() {
		return mouvSoldat;
	}

	public void setMouvSoldat(Entite mouvSoldat) {
		this.mouvSoldat = mouvSoldat;
	}

	public Boat getMouvBoat() {
		return mouvBoat;
	}

	public void setMouvBoat(Boat mouvBoat) {
		this.mouvBoat = mouvBoat;
	}

	public Triplet getBatiment() {
		return batiment;
	}

	public void setBatiment(Triplet batiment) {
		this.batiment = batiment;
	}

	public void changeMap(char key) {
		switch (key) {
		case 'a':
			Position p = new Position(0, 0);
			Main.grille.setP(p);
			break;

		case 'e':
			if (Main.plat == 1) {
				Main.plat = 0;
			} else {
				Main.plat = 1;
			}
			break;
		}
	}

	public void mort(Entite hab) {
		boolean delete = true;
		if (Main.perso1.isTourJoueur()) {
			for (Case cas : hab.getVisit()) {
				for (Entite ent : Main.perso1.getEntite()) {
					if (ent.getPosition() != hab.getPosition()) {
						for (Case cas2 : ent.getVisit()) {
							if (cas2.getPos() == cas.getPos()) {
								delete = false;
							}
						}
					}
				}
				if (delete)
					cas.setBuildable1(false);
				delete = true;
			}

		} else {
			for (Case cas : hab.getVisit()) {
				for (Entite ent : Main.perso2.getEntite()) {
					if (ent.getPosition() != hab.getPosition()) {
						for (Case cas2 : ent.getVisit()) {
							if (cas2.getPos() == cas.getPos()) {
								delete = false;
							}
						}
					}
				}
				if (delete)
					cas.setBuildable2(false);
				delete = true;
			}
		}
		if (hab instanceof Habitant) {
			if (((Habitant) hab).isWork()) {
				if (hab instanceof Fermier) {
					((Ferme) Main.grille.getGrille()[hab.getPosition().getA()][hab.getPosition().getB()][hab
							.getPosition().getC()].isOccuped()).setFermier(
									((Ferme) Main.grille.getGrille()[hab.getPosition().getA()][hab.getPosition()
											.getB()][hab.getPosition().getC()].isOccuped()).getFermier() - 1);
					((Habitant) hab).getEmbauche().setHab(((Habitant) hab).getEmbauche().getHab() - 1);

					((Fermier) hab).getTravaille().destruction();
				}
				if (hab instanceof Mineur) {
					((Mine) Main.grille.getGrille()[hab.getPosition().getA()][hab.getPosition().getB()][hab
							.getPosition().getC()].isOccuped()).setMineur(
									((Mine) Main.grille.getGrille()[hab.getPosition().getA()][hab.getPosition()
											.getB()][hab.getPosition().getC()].isOccuped()).getMineur() - 1);
					((Habitant) hab).getEmbauche().setHab(((Habitant) hab).getEmbauche().getHab() - 1);
				}
				if (hab instanceof Marin) {
					((Porte) Main.grille.getGrille()[hab.getPosition().getA()][hab.getPosition().getB()][hab
							.getPosition().getC()].isOccuped()).setMarin(
									((Porte) Main.grille.getGrille()[hab.getPosition().getA()][hab.getPosition()
											.getB()][hab.getPosition().getC()].isOccuped()).getMarin() - 1);
					((Habitant) hab).getEmbauche().setHab(((Habitant) hab).getEmbauche().getHab() - 1);

					((Marin) hab).getTravaille().destruction();
				}
				if (hab instanceof Soldat) {
					((Habitant) hab).getEmbauche().setHab(((Habitant) hab).getEmbauche().getHab() - 1);
				}
			} else {
				((Habitant) hab).getEmbauche().setHab(((Habitant) hab).getEmbauche().getHab() - 1);
			}
		}

	}

	public void addTri(Entite e) {
		int i = 0;
		boolean fin = false;
		if (Main.perso1.isTourJoueur()) {
			for (Entite ent : Main.perso1.getEntiteAff()) {
				if (!fin && ent != null) {
					if (ent.getPos().getY() < e.getPos().getY()) {
						fin = true;
						i--;
					}
					i++;
				}
			}
			Main.perso1.getEntiteAff().add(i, e);
		} else {
			for (Entite ent : Main.perso2.getEntiteAff()) {
				if (!fin && ent != null) {
					if (ent.getPos().getY() < e.getPos().getY()) {
						fin = true;
						i--;
					}
					i++;
				}
			}
			Main.perso2.getEntiteAff().add(i, e);
		}
	}

	public void bougeBoat(Boat b, double x, double y) {
		if (b instanceof Boat) {
			for (int i = 0; i < Main.grille.getX(); i++) {
				for (int j = 0; j < Main.grille.getY(); j++) {
					if (Main.posGrille[i][j][0].getX() >= x - 0.025 && Main.posGrille[i][j][0].getX() <= x + 0.025
							&& Main.posGrille[i][j][0].getY() >= y - 0.025
							&& Main.posGrille[i][j][0].getY() <= y + 0.025) {

						Triplet t = new Triplet(i, j, 0);
						if (Main.grille.getGrille()[t.getA()][t.getB()][t.getC()].isOccuped() == null
								&& Main.grille.getGrille()[t.getA()][t.getB()][t.getC()].getDecord() == 1) {

							if (t.getC() == b.getPosition().getC()) {
								if (Math.abs((t.getA() - b.getPosition().getA())) <= b.getMouv()
										&& Math.abs((t.getB() - b.getPosition().getB())) <= b.getMouv()) {
									if (touche.hasFinished()) {
										Main.grille.remouvMouv();
										Main.grille.getGrille()[t.getA()][t.getB()][t.getC()].setMouv(true);

										if (StdDraw.isMousePressed()) {
											Main.grille.getGrille()[b.getPosition().getA()][b.getPosition().getB()][b
													.getPosition().getC()].setOccuped(null);

											int depl = 0;

											if (Math.abs(((t.getA() - b.getPosition().getA()))) == 4) {
												depl = 4;
											} else if (Math.abs(((t.getB() - b.getPosition().getB()))) == 4) {
												depl = 4;
											} else if (Math.abs(((t.getA() - b.getPosition().getA()))) == 3) {
												depl = 3;
											} else if (Math.abs(((t.getB() - b.getPosition().getB()))) == 3) {
												depl = 3;
											} else if (Math.abs(((t.getA() - b.getPosition().getA()))) == 2) {
												depl = 2;
											} else if (Math.abs(((t.getB() - b.getPosition().getB()))) == 2) {
												depl = 2;
											} else {
												depl = 1;
											}

											b.setPos(Main.posGrille[i][j][0]);
											b.setPosition(t);

											for (Soldat s : b.getPassager()) {
												s.setPos(Main.posGrille[i][j][0]);
												s.setPosition(t);
											}

											b.setMouv(b.getMouv() - depl);
											Main.grille.getGrille()[t.getA()][t.getB()][t.getC()].setOccuped(b);
											Main.grille.getGrille()[t.getA()][t.getB()][t.getC()].setMouv(false);
											mouvBoat = null;
											Main.grille.remouvMouv();
											touche.restart();
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	public void bouge(Entite e, double x, double y) {
		if (e instanceof Soldat) {
			for (int i = 0; i < Main.grille.getX(); i++) {
				for (int j = 0; j < Main.grille.getY(); j++) {
					if (Main.posGrille[i][j][0].getX() >= x - 0.025 && Main.posGrille[i][j][0].getX() <= x + 0.025
							&& Main.posGrille[i][j][0].getY() >= y - 0.025
							&& Main.posGrille[i][j][0].getY() <= y + 0.025) {

						Triplet t = new Triplet(i, j, 0);
						if (Main.grille.getGrille()[t.getA()][t.getB()][t.getC()].isOccuped() == null
								&& Main.grille.getGrille()[t.getA()][t.getB()][t.getC()].getDecord() == 0) {

							if (((Soldat) e).isNavig() == null) {

								if (t.getC() == e.getPosition().getC()) {
									if (Math.abs((t.getA() - e.getPosition().getA())) <= ((Soldat) e).getMouv() && Math
											.abs((t.getB() - e.getPosition().getB())) <= ((Soldat) e).getMouv()) {
										if (touche.hasFinished()) {
											Main.grille.remouvMouv();
											Main.grille.getGrille()[t.getA()][t.getB()][t.getC()].setMouv(true);

											if (StdDraw.isMousePressed()) {
												Main.grille.getGrille()[e.getPosition().getA()][e.getPosition()
														.getB()][e.getPosition().getC()].setOccuped(null);

												int depl = 0;

												if (Math.abs(((t.getA() - e.getPosition().getA()))) == 2) {
													depl = 2;
												} else if (Math.abs(((t.getB() - e.getPosition().getB()))) == 2) {
													depl = 2;
												} else {
													depl = 1;
												}

												e.setPos(Main.posGrille[i][j][0]);
												e.setPosition(t);

												((Soldat) e).setMouv(((Soldat) e).getMouv() - depl);
												Main.grille.getGrille()[t.getA()][t.getB()][t.getC()].setOccuped(e);
												Main.grille.getGrille()[t.getA()][t.getB()][t.getC()].setMouv(false);
												mouvSoldat = null;
												Main.grille.remouvMouv();
												touche.restart();
											}
										}
									}
								} else if (t.getC() == 1 + e.getPosition().getC()) {

									// TODO bouger en montagne

									if (Math.abs((t.getA() - e.getPosition().getA())) <= 1
											&& Math.abs((t.getB() - e.getPosition().getB())) <= 1) {
										if (touche.hasFinished()) {
											Main.grille.remouvMouv();
											Main.grille.getGrille()[t.getA()][t.getB()][t.getC()].setMouv(true);

											if (StdDraw.isMousePressed()) {
												Main.grille.getGrille()[e.getPosition().getA()][e.getPosition()
														.getB()][e.getPosition().getC()].setOccuped(null);
												e.setPos(Main.posGrille[i][j][0]);
												e.setPosition(t);

												((Soldat) e).setMouv(0);
												Main.grille.getGrille()[t.getA()][t.getB()][t.getC()].setOccuped(e);
												Main.grille.getGrille()[t.getA()][t.getB()][t.getC()].setMouv(false);
												mouvSoldat = null;
												Main.grille.remouvMouv();
												touche.restart();
											}
										}
									}
								}
							} else {
								if (t.getC() == e.getPosition().getC()) {
									if (Math.abs((t.getA() - e.getPosition().getA())) <= ((Soldat) e).getMouv() && Math
											.abs((t.getB() - e.getPosition().getB())) <= ((Soldat) e).getMouv()) {
										if (touche.hasFinished()) {
											Main.grille.remouvMouv();
											Main.grille.getGrille()[t.getA()][t.getB()][t.getC()].setMouv(true);

											if (StdDraw.isMousePressed()) {

												((Soldat) e).isNavig().getPassager().remove(e);
												((Soldat) e).setNavig(null);

												e.setPos(Main.posGrille[i][j][0]);
												e.setPosition(t);

												((Soldat) e).setMouv(0);
												Main.grille.getGrille()[t.getA()][t.getB()][t.getC()].setOccuped(e);
												Main.grille.getGrille()[t.getA()][t.getB()][t.getC()].setMouv(false);
												mouvSoldat = null;
												Main.grille.remouvMouv();
												touche.restart();
											}
										}
									}
								}
							}
						} else if (Main.grille.getGrille()[t.getA()][t.getB()][t.getC()].isOccuped() instanceof Boat
								&& ((Soldat) e).isNavig() == null) {
							if (t.getC() == e.getPosition().getC()) {
								if (Math.abs((t.getA() - e.getPosition().getA())) <= ((Soldat) e).getMouv()
										&& Math.abs((t.getB() - e.getPosition().getB())) <= ((Soldat) e).getMouv()) {
									if (touche.hasFinished()) {
										Main.grille.remouvMouv();
										Main.grille.getGrille()[t.getA()][t.getB()][t.getC()].setMouv(true);

										if (StdDraw.isMousePressed()) {

											if (((Boat) Main.grille.getGrille()[t.getA()][t.getB()][t.getC()]
													.isOccuped()).getPassager().size() < 4) {

												Main.grille.getGrille()[e.getPosition().getA()][e.getPosition()
														.getB()][e.getPosition().getC()].setOccuped(null);
												((Boat) Main.grille.getGrille()[t.getA()][t.getB()][t.getC()]
														.isOccuped()).getPassager().add((Soldat) e);
												((Soldat) e).setNavig(
														((Boat) Main.grille.getGrille()[t.getA()][t.getB()][t.getC()]
																.isOccuped()));
												e.setPos(Main.posGrille[i][j][0]);
												e.setPosition(t);
												((Soldat) e).setMouv(0);
												Main.grille.getGrille()[t.getA()][t.getB()][t.getC()].setMouv(false);
												mouvSoldat = null;
												Main.grille.remouvMouv();
												touche.restart();
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	public void attaque(Entite s, double x, double y) {
		if (s instanceof Soldat) {
			if (touche.hasFinished() && ((Soldat) s).getMouv() > 0) {
				for (int i = 0; i < Main.grille.getX(); i++) {
					for (int j = 0; j < Main.grille.getY(); j++) {
						if (Main.posGrille[i][j][0].getX() >= x - 0.025 && Main.posGrille[i][j][0].getX() <= x + 0.025
								&& Main.posGrille[i][j][0].getY() >= y - 0.025
								&& Main.posGrille[i][j][0].getY() <= y + 0.025) {

							Triplet t = new Triplet(i, j, 0);
							if (Main.grille.getGrille()[t.getA()][t.getB()][t.getC()].isOccuped() != null
									&& Main.grille.getGrille()[t.getA()][t.getB()][t.getC()].isOccuped()
											.getJoueur() != s.getJoueur()) {

								if (t.getC() == s.getPosition().getC()) {
									if (Math.abs((t.getA() - s.getPosition().getA())) <= 1
											&& Math.abs((t.getB() - s.getPosition().getB())) <= 1) {

										Main.grille.getGrille()[t.getA()][t.getB()][t.getC()].isOccuped()
												.setVie(Main.grille.getGrille()[t.getA()][t.getB()][t.getC()]
														.isOccuped().getVie() - ((Soldat) s).getAtt());
										
										if(Main.grille.getGrille()[t.getA()][t.getB()][t.getC()].isOccuped()
												.getVie() <= 0) {
											if(Main.perso1.isTourJoueur()) {
												if (!(Main.perso2.getEntite().get(Main.perso2.getEntite().size() - 1) instanceof Habitant)
														|| Main.perso2.getEntite().get(Main.perso2.getEntite().size() - 1) instanceof Soldat) {
													Main.perso2.getEntiteAff().remove(Main.perso2.getEntiteAff()
															.indexOf(Main.perso2.getEntite().get(Main.perso2.getEntite().size() - 1)));
												}

												mort(Main.perso1.getEntite().get(Main.perso1.getEntite().size() - 1));
												Main.perso1.getEntite().remove(Main.perso1.getEntite().size() - 1);
											}else {
												if (!(Main.perso1.getEntite().get(Main.perso1.getEntite().size() - 1) instanceof Habitant)
														|| Main.perso1.getEntite().get(Main.perso1.getEntite().size() - 1) instanceof Soldat) {
													Main.perso1.getEntiteAff().remove(Main.perso1.getEntiteAff()
															.indexOf(Main.perso1.getEntite().get(Main.perso1.getEntite().size() - 1)));
												}

												mort(Main.perso1.getEntite().get(Main.perso1.getEntite().size() - 1));
												Main.perso1.getEntite().remove(Main.perso1.getEntite().size() - 1);
											}
										}

										Main.grille.remouvMouv();
										((Soldat) s).setMouv(0);
										touche.restart();
									}
								}
							}
						}
					}
				}
			}
		}
	}

	private void reset() {

		List<String> l = new ArrayList<String>();
		l.add("ferme");
		l.add("mine");
		l.add("auberge");
		l.add("port");
		l.add("tour");
		Main.onglet.setOnglet(l);
		Main.onglet.setType("build");

		touche.restart();

		buildFerme = false;
		buildMine = false;
		buildAuberge = false;
		buildPort = false;
		buildTour = false;
		buildChamp = false;
		buildBoat1 = false;

		leSoldat = null;
		mouvSoldat = null;
		mouvBoat = null;
		batiment = null;
	}

	public void action(double x, double y) {
		if (touche.hasFinished()) {

			if (0.5 >= x - 0.075 && 0.5 <= x + 0.075 && 0.9 >= y - 0.095 && 0.9 <= y - 0.045) {
				if (Main.perso1.isTourJoueur()) {

					for (Entite ent : Main.perso1.getEntite()) {
						if (ent instanceof Ferme) {
							Main.perso1.setFood(Main.perso1.getFood() + ((Ferme) ent).production());
						}
						if (ent instanceof Mine) {
							Main.perso1.setOr(Main.perso1.getOr() + ((Mine) ent).production());
						}
						if (ent instanceof Soldat) {
							((Soldat) ent).retournMouv();
						}
						if (ent instanceof Boat) {
							((Boat) ent).retournMouv();
						}
					}
					int a = Main.perso1.getEntite().size() * 10;
					Main.perso1.setFood(Main.perso1.getFood() - a);
					while (Main.perso1.getFood() < 0) {
						Main.perso1.setFood(Main.perso1.getFood() + 10);

						if (!(Main.perso1.getEntite().get(Main.perso1.getEntite().size() - 1) instanceof Habitant)
								|| Main.perso1.getEntite().get(Main.perso1.getEntite().size() - 1) instanceof Soldat) {
							Main.perso1.getEntiteAff().remove(Main.perso1.getEntiteAff()
									.indexOf(Main.perso1.getEntite().get(Main.perso1.getEntite().size() - 1)));
						}

						mort(Main.perso1.getEntite().get(Main.perso1.getEntite().size() - 1));
						Main.perso1.getEntite().remove(Main.perso1.getEntite().size() - 1);
					}

					reset();

					Main.perso1.setTourJoueur(false);
					Main.perso2.setTourJoueur(true);

				} else {

					for (Entite ent : Main.perso2.getEntite()) {
						if (ent instanceof Ferme) {
							Main.perso2.setFood(Main.perso2.getFood() + ((Ferme) ent).production());
						}
						if (ent instanceof Mine) {
							Main.perso2.setOr(Main.perso2.getOr() + ((Mine) ent).production());
						}
						if (ent instanceof Soldat) {
							((Soldat) ent).retournMouv();
						}
						if (ent instanceof Boat) {
							((Boat) ent).retournMouv();
						}
					}
					int a = Main.perso2.getEntite().size() * 10;
					Main.perso2.setFood(Main.perso2.getFood() - a);
					while (Main.perso2.getFood() < 0) {
						Main.perso2.setFood(Main.perso2.getFood() + 10);

						if (!(Main.perso2.getEntite().get(Main.perso2.getEntite().size() - 1) instanceof Habitant)
								|| Main.perso2.getEntite().get(Main.perso2.getEntite().size() - 1) instanceof Soldat) {
							Main.perso2.getEntiteAff().remove(Main.perso2.getEntiteAff()
									.indexOf(Main.perso2.getEntite().get(Main.perso2.getEntite().size() - 1)));
						}

						mort(Main.perso2.getEntite().get(Main.perso2.getEntite().size() - 1));
						Main.perso2.getEntite().remove(Main.perso2.getEntite().size() - 1);
					}

					reset();

					Main.perso1.setTourJoueur(true);
					Main.perso2.setTourJoueur(false);

				}

				Main.ath.SetImage("change");
				touche.restart();
			}
			if (Main.perso1.isTourJoueur()) {

				for (int i = 0; i < Main.grille.getX(); i++) {
					for (int j = 0; j < Main.grille.getY(); j++) {

						if (Main.posGrille[i][j][0].getX() >= x - 0.025 && Main.posGrille[i][j][0].getX() <= x + 0.025
								&& Main.posGrille[i][j][0].getY() >= y - 0.025
								&& Main.posGrille[i][j][0].getY() <= y + 0.025) {

							if (Main.grille.getGrille()[i][j][0].isOccuped() instanceof Boat && mouvSoldat == null) {
								if (Main.grille.getGrille()[i][j][0].isOccuped().getJoueur() == 1) {
									if (((Boat) Main.grille.getGrille()[i][j][0].isOccuped()).getMouv() != 0) {
										if (mouvBoat == null) {
											mouvBoat = (Boat) Main.grille.getGrille()[i][j][0].isOccuped();

											List<String> l = new ArrayList<String>(5);

											for (Soldat s : mouvBoat.getPassager()) {
												l.add(s.getNom());
											}

											while (l.size() < 5) {
												l.add(null);
											}

											l.add(4, "retour");

											Main.onglet.setOnglet(l);
											Main.onglet.setType("boat");

										} else {

											List<String> l = new ArrayList<String>();
											l.add("ferme");
											l.add("mine");
											l.add("auberge");
											l.add("port");
											l.add("tour");
											Main.onglet.setOnglet(l);
											Main.onglet.setType("build");

											Main.grille.remouvMouv();
											mouvBoat = null;
										}
										touche.restart();
									}
								}
							}
						}
					}
				}

				for (int i = 0; i < Main.grille.getX(); i++) {
					for (int j = 0; j < Main.grille.getY(); j++) {

						if (Main.posGrille[i][j][0].getX() >= x - 0.025 && Main.posGrille[i][j][0].getX() <= x + 0.025
								&& Main.posGrille[i][j][0].getY() >= y - 0.025
								&& Main.posGrille[i][j][0].getY() <= y + 0.025) {

							if (Main.grille.getGrille()[i][j][0].isOccuped() instanceof Soldat) {
								if (Main.grille.getGrille()[i][j][0].isOccuped().getJoueur() == 1) {
									if (((Soldat) Main.grille.getGrille()[i][j][0].isOccuped()).getMouv() != 0) {
										if (mouvSoldat == null) {
											mouvSoldat = Main.grille.getGrille()[i][j][0].isOccuped();
										} else {
											Main.grille.remouvMouv();
											mouvSoldat = null;
										}
										touche.restart();
									}
								}
							}
						}
					}
				}

				for (int i = 0; i < Main.grille.getX(); i++) {
					for (int j = 0; j < Main.grille.getY(); j++) {

						if (Main.posGrille[i][j][0].getX() >= x - 0.025 && Main.posGrille[i][j][0].getX() <= x + 0.025
								&& Main.posGrille[i][j][0].getY() >= y - 0.025
								&& Main.posGrille[i][j][0].getY() <= y + 0.025) {

							if (Main.grille.getGrille()[i][j][0].isOccuped() instanceof Ferme) {
								if (Main.grille.getGrille()[i][j][0].isOccuped().getJoueur() == 1) {
									if (((Ferme) Main.grille.getGrille()[i][j][0].isOccuped()).getFermier() < 3) {
										List<String> l = new ArrayList<String>();
										l.add("champ1");
										l.add("champ1");
										l.add("champ1");
										l.add("champ1");
										l.add("retour");
										Main.onglet.setOnglet(l);
										Main.onglet.setType("ferme");

										batiment = new Triplet(i, j, 0);

										touche.restart();
									}
								}
							}
						}
					}
				}

				for (int i = 0; i < Main.grille.getX(); i++) {
					for (int j = 0; j < Main.grille.getY(); j++) {

						if (Main.posGrille[i][j][0].getX() >= x - 0.025 && Main.posGrille[i][j][0].getX() <= x + 0.025
								&& Main.posGrille[i][j][0].getY() >= y - 0.025
								&& Main.posGrille[i][j][0].getY() <= y + 0.025) {

							if (Main.grille.getGrille()[i][j][0].isOccuped() instanceof Caserne) {
								if (Main.grille.getGrille()[i][j][0].isOccuped().getJoueur() == 1) {
									List<String> l = new ArrayList<String>();
									l.add("fermier");
									l.add("mineur");
									l.add("soldat");
									l.add("marin");
									l.add("retour");
									Main.onglet.setOnglet(l);
									Main.onglet.setType("auberge");

									batiment = new Triplet(i, j, 0);

									touche.restart();
								}
							}
						}

					}
				}

				for (int i = 0; i < Main.grille.getX(); i++) {
					for (int j = 0; j < Main.grille.getY(); j++) {

						if (Main.posGrille[i][j][0].getX() >= x - 0.025 && Main.posGrille[i][j][0].getX() <= x + 0.025
								&& Main.posGrille[i][j][0].getY() >= y - 0.025
								&& Main.posGrille[i][j][0].getY() <= y + 0.025) {

							if (Main.grille.getGrille()[i][j][0].isOccuped() instanceof Porte) {
								if (Main.grille.getGrille()[i][j][0].isOccuped().getJoueur() == 1) {
									List<String> l = new ArrayList<String>();
									l.add("bateauPetit");
									l.add("bateauPetit");
									l.add("bateauPetit");
									l.add("bateauPetit");
									l.add("retour");
									Main.onglet.setOnglet(l);
									Main.onglet.setType("port");

									batiment = new Triplet(i, j, 0);

									touche.restart();
								}
							}
						}

					}
				}
			} else {

				for (int i = 0; i < Main.grille.getX(); i++) {
					for (int j = 0; j < Main.grille.getY(); j++) {

						if (Main.posGrille[i][j][0].getX() >= x - 0.025 && Main.posGrille[i][j][0].getX() <= x + 0.025
								&& Main.posGrille[i][j][0].getY() >= y - 0.025
								&& Main.posGrille[i][j][0].getY() <= y + 0.025) {

							if (Main.grille.getGrille()[i][j][0].isOccuped() instanceof Boat && mouvSoldat == null) {
								if (Main.grille.getGrille()[i][j][0].isOccuped().getJoueur() == 2) {
									if (((Boat) Main.grille.getGrille()[i][j][0].isOccuped()).getMouv() != 0) {
										if (mouvBoat == null) {
											mouvBoat = (Boat) Main.grille.getGrille()[i][j][0].isOccuped();

											List<String> l = new ArrayList<String>(5);

											for (Soldat s : mouvBoat.getPassager()) {
												l.add(s.getNom());
											}

											while (l.size() < 5) {
												l.add(null);
											}

											l.add(4, "retour");

											Main.onglet.setOnglet(l);
											Main.onglet.setType("boat");

										} else {

											List<String> l = new ArrayList<String>();
											l.add("ferme");
											l.add("mine");
											l.add("auberge");
											l.add("port");
											l.add("tour");
											Main.onglet.setOnglet(l);
											Main.onglet.setType("build");

											Main.grille.remouvMouv();
											mouvBoat = null;
										}
										touche.restart();
									}
								}
							}
						}
					}
				}

				for (int i = 0; i < Main.grille.getX(); i++) {
					for (int j = 0; j < Main.grille.getY(); j++) {

						if (Main.posGrille[i][j][0].getX() >= x - 0.025 && Main.posGrille[i][j][0].getX() <= x + 0.025
								&& Main.posGrille[i][j][0].getY() >= y - 0.025
								&& Main.posGrille[i][j][0].getY() <= y + 0.025) {

							if (Main.grille.getGrille()[i][j][0].isOccuped() instanceof Soldat) {
								if (Main.grille.getGrille()[i][j][0].isOccuped().getJoueur() == 2) {
									if (((Soldat) Main.grille.getGrille()[i][j][0].isOccuped()).getMouv() != 0) {
										if (mouvSoldat == null) {
											mouvSoldat = (Soldat) Main.grille.getGrille()[i][j][0].isOccuped();
										} else {
											Main.grille.remouvMouv();
											mouvSoldat = null;
										}
										touche.restart();
									}
								}
							}
						}
					}
				}

				for (int i = 0; i < Main.grille.getX(); i++) {
					for (int j = 0; j < Main.grille.getY(); j++) {

						if (Main.posGrille[i][j][0].getX() >= x - 0.025 && Main.posGrille[i][j][0].getX() <= x + 0.025
								&& Main.posGrille[i][j][0].getY() >= y - 0.025
								&& Main.posGrille[i][j][0].getY() <= y + 0.025) {

							if (Main.grille.getGrille()[i][j][0].isOccuped() instanceof Ferme) {
								if (Main.grille.getGrille()[i][j][0].isOccuped().getJoueur() == 2) {
									if (((Ferme) Main.grille.getGrille()[i][j][0].isOccuped()).getFermier() < 3) {
										List<String> l = new ArrayList<String>();
										l.add("champ1");
										l.add("champ1");
										l.add("champ1");
										l.add("champ1");
										l.add("retour");
										Main.onglet.setOnglet(l);
										Main.onglet.setType("ferme");

										batiment = new Triplet(i, j, 0);

										touche.restart();
									}
								}
							}
						}
					}
				}

				for (int i = 0; i < Main.grille.getX(); i++) {
					for (int j = 0; j < Main.grille.getY(); j++) {

						if (Main.posGrille[i][j][0].getX() >= x - 0.025 && Main.posGrille[i][j][0].getX() <= x + 0.025
								&& Main.posGrille[i][j][0].getY() >= y - 0.025
								&& Main.posGrille[i][j][0].getY() <= y + 0.025) {

							if (Main.grille.getGrille()[i][j][0].isOccuped() instanceof Caserne) {
								if (Main.grille.getGrille()[i][j][0].isOccuped().getJoueur() == 2) {
									List<String> l = new ArrayList<String>();
									l.add("fermier");
									l.add("mineur");
									l.add("soldat");
									l.add("marin");
									l.add("retour");
									Main.onglet.setOnglet(l);
									Main.onglet.setType("auberge");

									batiment = new Triplet(i, j, 0);

									touche.restart();
								}
							}
						}

					}
				}

				for (int i = 0; i < Main.grille.getX(); i++) {
					for (int j = 0; j < Main.grille.getY(); j++) {

						if (Main.posGrille[i][j][0].getX() >= x - 0.025 && Main.posGrille[i][j][0].getX() <= x + 0.025
								&& Main.posGrille[i][j][0].getY() >= y - 0.025
								&& Main.posGrille[i][j][0].getY() <= y + 0.025) {

							if (Main.grille.getGrille()[i][j][0].isOccuped() instanceof Porte) {
								if (Main.grille.getGrille()[i][j][0].isOccuped().getJoueur() == 2) {
									List<String> l = new ArrayList<String>();
									l.add("bateauPetit");
									l.add("bateauPetit");
									l.add("bateauPetit");
									l.add("bateauPetit");
									l.add("retour");
									Main.onglet.setOnglet(l);
									Main.onglet.setType("port");

									batiment = new Triplet(i, j, 0);

									touche.restart();
								}
							}
						}

					}
				}
			}
		}
	}

	public void setSoldat(Soldat s, double x, double y) {
		if (!(0.85 <= x && x <= 0.98 && 0.49 <= y && y <= 1)) {
			if (s instanceof Guerrier) {
				if (Main.perso1.isTourJoueur()) {

					for (int i = 0; i < Main.grille.getX(); i++) {
						for (int j = 0; j < Main.grille.getY(); j++) {
							if (Main.posGrille[i][j][0].getX() >= x - 0.025
									&& Main.posGrille[i][j][0].getX() <= x + 0.025
									&& Main.posGrille[i][j][0].getY() >= y - 0.025
									&& Main.posGrille[i][j][0].getY() <= y + 0.025) {

								Triplet t = new Triplet(i, j, 0);
								if (Main.grille.getGrille()[i][j][0].isBuildable1()
										&& Main.grille.getGrille()[i][j][0].isOccuped() == null) {

									s.setPosition(t);
									addTri(s);
									Main.perso1.getEntite().add(s);
									leSoldat = null;
									Main.grille.getGrille()[i][j][0].setOccuped(s);
									Main.ath.SetImage("Srecru");
									Main.perso1.setOr(Main.perso1.getOr() - 50);
								}
							}
						}
					}
				} else {
					Main.perso2.setOr(Main.perso2.getOr() - 50);
					for (int i = 0; i < Main.grille.getX(); i++) {
						for (int j = 0; j < Main.grille.getY(); j++) {
							if (Main.posGrille[i][j][0].getX() >= x - 0.025
									&& Main.posGrille[i][j][0].getX() <= x + 0.025
									&& Main.posGrille[i][j][0].getY() >= y - 0.025
									&& Main.posGrille[i][j][0].getY() <= y + 0.025) {

								Triplet t = new Triplet(i, j, 0);
								if (Main.grille.getGrille()[i][j][0].isBuildable2()
										&& Main.grille.getGrille()[i][j][0].isOccuped() == null) {

									s.setPosition(t);
									addTri(s);
									Main.perso2.getEntite().add(s);
									leSoldat = null;
									Main.grille.getGrille()[i][j][0].setOccuped(s);
									Main.ath.SetImage("Srecru");
								}
							}
						}
					}
				}
			}
		}
	}

	public void construction(double x, double y) {
		if (touche.hasFinished()) {
			if (Main.onglet.getType() == "build") {
				if (0.85 <= x && x <= 0.98 && 0.89 < y && y <= 0.99) {
					if (Main.perso1.isTourJoueur()) {
						if (buildFerme) {
							Main.perso1.setOr(Main.perso1.getOr() + 100);
							buildFerme = !buildFerme;
							touche.restart();
						} else if (Main.perso1.getOr() >= 100) {
							Main.perso1.setOr(Main.perso1.getOr() - 100);
							buildFerme = !buildFerme;
							touche.restart();
						}
					} else {
						if (buildFerme) {
							Main.perso2.setOr(Main.perso2.getOr() + 100);
							buildFerme = !buildFerme;
							touche.restart();
						} else if (Main.perso2.getOr() >= 100) {
							Main.perso2.setOr(Main.perso2.getOr() - 100);
							buildFerme = !buildFerme;
							touche.restart();
						}
					}
				}
				if (0.85 <= x && x <= 0.98 && 0.79 < y && y <= 0.89) {
					if (Main.perso1.isTourJoueur()) {
						if (buildMine) {
							Main.perso1.setOr(Main.perso1.getOr() + 100);
							buildMine = !buildMine;
							touche.restart();
						} else if (Main.perso1.getOr() >= 100) {
							Main.perso1.setOr(Main.perso1.getOr() - 100);
							buildMine = !buildMine;
							touche.restart();
						}
					} else {
						if (buildMine) {
							Main.perso2.setOr(Main.perso2.getOr() + 100);
							buildMine = !buildMine;
							touche.restart();
						} else if (Main.perso2.getOr() >= 100) {
							Main.perso2.setOr(Main.perso2.getOr() - 100);
							buildMine = !buildMine;
							touche.restart();
						}
					}
				}
				if (0.85 <= x && x <= 0.98 && 0.69 < y && y <= 0.79) {
					if (Main.perso1.isTourJoueur()) {
						if (buildAuberge) {
							Main.perso1.setOr(Main.perso1.getOr() + 100);
							buildAuberge = !buildAuberge;
							touche.restart();
						} else if (Main.perso1.getOr() >= 100) {
							Main.perso1.setOr(Main.perso1.getOr() - 100);
							buildAuberge = !buildAuberge;
							touche.restart();
						}
					} else {
						if (buildAuberge) {
							Main.perso2.setOr(Main.perso2.getOr() + 100);
							buildAuberge = !buildAuberge;
							touche.restart();
						} else if (Main.perso2.getOr() >= 100) {
							Main.perso2.setOr(Main.perso2.getOr() - 100);
							buildAuberge = !buildAuberge;
							touche.restart();
						}
					}
				}
				if (0.85 <= x && x <= 0.98 && 0.59 < y && y <= 0.69) {
					if (Main.perso1.isTourJoueur()) {
						if (buildPort) {
							Main.perso1.setOr(Main.perso1.getOr() + 200);
							buildPort = !buildPort;
							touche.restart();
						} else if (Main.perso1.getOr() >= 200) {
							Main.perso1.setOr(Main.perso1.getOr() - 200);
							buildPort = !buildPort;
							touche.restart();
						}
					} else {
						if (buildPort) {
							Main.perso2.setOr(Main.perso2.getOr() + 200);
							buildPort = !buildPort;
							touche.restart();
						} else if (Main.perso2.getOr() >= 200) {
							Main.perso2.setOr(Main.perso2.getOr() - 200);
							buildPort = !buildPort;
							touche.restart();
						}
					}
				}
				if (0.85 <= x && x <= 0.98 && 0.49 < y && y <= 0.59) {
					if (Main.perso1.isTourJoueur()) {
						if (buildTour) {
							Main.perso1.setOr(Main.perso1.getOr() + 100);
							buildTour = !buildTour;
							touche.restart();
						} else if (Main.perso1.getOr() >= 100) {
							Main.perso1.setOr(Main.perso1.getOr() - 100);
							buildTour = !buildTour;
							touche.restart();
						}
					} else {
						if (buildTour) {
							Main.perso2.setOr(Main.perso2.getOr() + 100);
							buildTour = !buildTour;
							touche.restart();
						} else if (Main.perso2.getOr() >= 100) {
							Main.perso2.setOr(Main.perso2.getOr() - 100);
							buildTour = !buildTour;
							touche.restart();
						}
					}
				}
			} else if (Main.onglet.getType() == "auberge") {
				if (0.85 <= x && x <= 0.98 && 0.89 < y && y <= 0.99) {
					if (Main.perso1.isTourJoueur()) {
						if (Main.perso1.getOr() >= 50) {
							if (((Caserne) Main.grille.getGrille()[batiment.getA()][batiment.getB()][0].isOccuped())
									.getHab() < 10) {
								((Caserne) Main.grille.getGrille()[batiment.getA()][batiment.getB()][0].isOccuped())
										.setHab(((Caserne) Main.grille.getGrille()[batiment.getA()][batiment.getB()][0]
												.isOccuped()).getHab() + 1);
								Caserne cas = ((Caserne) Main.grille.getGrille()[batiment.getA()][batiment.getB()][0]
										.isOccuped());
								Habitant fermier = new Fermier("ferme", batiment, cas, null, 1);
								Main.perso1.getEntite().add(fermier);
								Main.perso1.setOr(Main.perso1.getOr() - 50);
								Main.ath.SetImage("Frecru");
								touche.restart();
							}
						}
					} else {
						if (Main.perso2.getOr() >= 50) {
							if (((Caserne) Main.grille.getGrille()[batiment.getA()][batiment.getB()][0].isOccuped())
									.getHab() < 10) {
								((Caserne) Main.grille.getGrille()[batiment.getA()][batiment.getB()][0].isOccuped())
										.setHab(((Caserne) Main.grille.getGrille()[batiment.getA()][batiment.getB()][0]
												.isOccuped()).getHab() + 1);
								Caserne cas = ((Caserne) Main.grille.getGrille()[batiment.getA()][batiment.getB()][0]
										.isOccuped());
								Habitant fermier = new Fermier("fermier", batiment, cas, null, 2);
								Main.perso2.getEntite().add(fermier);
								Main.perso2.setOr(Main.perso2.getOr() - 50);
								Main.ath.SetImage("Frecru");
								touche.restart();
							}
						}
					}

				}
				if (0.85 <= x && x <= 0.98 && 0.79 < y && y <= 0.89) {

					if (Main.perso1.isTourJoueur()) {
						if (Main.perso1.getOr() >= 50) {
							if (((Caserne) Main.grille.getGrille()[batiment.getA()][batiment.getB()][0].isOccuped())
									.getHab() < 10) {
								((Caserne) Main.grille.getGrille()[batiment.getA()][batiment.getB()][0].isOccuped())
										.setHab(((Caserne) Main.grille.getGrille()[batiment.getA()][batiment.getB()][0]
												.isOccuped()).getHab() + 1);
								Caserne cas = ((Caserne) Main.grille.getGrille()[batiment.getA()][batiment.getB()][0]
										.isOccuped());
								Habitant mineur = new Mineur("mineur", batiment, cas, 1);
								Main.perso1.setOr(Main.perso1.getOr() - 50);
								boolean fin = false;

								for (int i = 0; i < Main.grille.getX(); i++) {
									for (int j = 0; j < Main.grille.getY(); j++) {
										for (int k = 0; k < Main.grille.getZ(); k++) {
											if (Main.grille.getGrille()[i][j][k] != null && !fin) {
												if (Main.grille.getGrille()[i][j][k].isOccuped() instanceof Mine) {
													if (((Mine) Main.grille.getGrille()[i][j][k].isOccuped())
															.getMineur() < 5
															&& ((Mine) Main.grille.getGrille()[i][j][k].isOccuped())
																	.getJoueur() == 1) {
														((Mine) Main.grille.getGrille()[i][j][k].isOccuped()).setMineur(
																((Mine) Main.grille.getGrille()[i][j][k].isOccuped())
																		.getMineur() + 1);
														mineur.setPosition(new Triplet(i, j, k));
														mineur.setWork(true);
														fin = true;
													}
												}
											}
										}
									}
								}

								Main.perso1.getEntite().add(mineur);
								Main.ath.SetImage("Mirecru");

								touche.restart();
							}
						}
					} else {
						if (Main.perso2.getOr() >= 50) {
							if (((Caserne) Main.grille.getGrille()[batiment.getA()][batiment.getB()][0].isOccuped())
									.getHab() < 10) {
								((Caserne) Main.grille.getGrille()[batiment.getA()][batiment.getB()][0].isOccuped())
										.setHab(((Caserne) Main.grille.getGrille()[batiment.getA()][batiment.getB()][0]
												.isOccuped()).getHab() + 1);
								Caserne cas = ((Caserne) Main.grille.getGrille()[batiment.getA()][batiment.getB()][0]
										.isOccuped());
								Habitant mineur = new Mineur("mineur", batiment, cas, 2);
								Main.perso2.setOr(Main.perso2.getOr() - 50);
								boolean fin = false;

								for (int i = 0; i < Main.grille.getX(); i++) {
									for (int j = 0; j < Main.grille.getY(); j++) {
										for (int k = 0; k < Main.grille.getZ(); k++) {
											if (Main.grille.getGrille()[i][j][k] != null && !fin) {
												if (Main.grille.getGrille()[i][j][k].isOccuped() instanceof Mine) {
													if (((Mine) Main.grille.getGrille()[i][j][k].isOccuped())
															.getMineur() < 5
															&& ((Mine) Main.grille.getGrille()[i][j][k].isOccuped())
																	.getJoueur() == 2) {
														((Mine) Main.grille.getGrille()[i][j][k].isOccuped()).setMineur(
																((Mine) Main.grille.getGrille()[i][j][k].isOccuped())
																		.getMineur() + 1);
														mineur.setPosition(new Triplet(i, j, k));
														mineur.setWork(true);
														fin = true;
													}
												}
											}
										}
									}
								}

								Main.perso2.getEntite().add(mineur);
								Main.ath.SetImage("Mirecru");

								touche.restart();
							}
						}
					}
				}
				if (0.85 <= x && x <= 0.98 && 0.69 < y && y <= 0.79) {
					if (((Caserne) Main.grille.getGrille()[batiment.getA()][batiment.getB()][0].isOccuped())
							.getHab() < 10) {
						List<String> l = new ArrayList<String>();
						l.add("guerrier");
						l.add("archer");
						l.add("constructeur");
						l.add("suivant");
						l.add("retour");
						Main.onglet.setOnglet(l);
						Main.onglet.setType("soldat1");
						touche.restart();
					}
				}
				if (0.85 <= x && x <= 0.98 && 0.59 < y && y <= 0.69) {
					if (Main.perso1.isTourJoueur()) {
						if (Main.perso1.getOr() >= 50) {
							if (((Caserne) Main.grille.getGrille()[batiment.getA()][batiment.getB()][0].isOccuped())
									.getHab() < 10) {
								((Caserne) Main.grille.getGrille()[batiment.getA()][batiment.getB()][0].isOccuped())
										.setHab(((Caserne) Main.grille.getGrille()[batiment.getA()][batiment.getB()][0]
												.isOccuped()).getHab() + 1);
								Caserne cas = ((Caserne) Main.grille.getGrille()[batiment.getA()][batiment.getB()][0]
										.isOccuped());
								Habitant marin = new Marin("marin", batiment, cas, null, 1);
								Main.perso1.setOr(Main.perso1.getOr() - 50);
								Main.perso1.getEntite().add(marin);
								Main.ath.SetImage("Marecru");
								touche.restart();
							}
						}
					} else {
						if (Main.perso2.getOr() >= 50) {
							if (((Caserne) Main.grille.getGrille()[batiment.getA()][batiment.getB()][0].isOccuped())
									.getHab() < 10) {
								((Caserne) Main.grille.getGrille()[batiment.getA()][batiment.getB()][0].isOccuped())
										.setHab(((Caserne) Main.grille.getGrille()[batiment.getA()][batiment.getB()][0]
												.isOccuped()).getHab() + 1);
								Caserne cas = ((Caserne) Main.grille.getGrille()[batiment.getA()][batiment.getB()][0]
										.isOccuped());
								Habitant marin = new Marin("marin", batiment, cas, null, 2);
								Main.perso2.setOr(Main.perso2.getOr() - 50);
								Main.perso2.getEntite().add(marin);
								Main.ath.SetImage("Marecru");
								touche.restart();
							}
						}
					}
				}
				if (0.85 <= x && x <= 0.98 && 0.49 < y && y <= 0.59) {
					List<String> l = new ArrayList<String>();
					l.add("ferme");
					l.add("mine");
					l.add("auberge");
					l.add("port");
					l.add("tour");
					Main.onglet.setOnglet(l);
					Main.onglet.setType("build");
					batiment = null;
					touche.restart();
				}
			} else if (Main.onglet.getType() == "soldat1") {
				if (0.85 <= x && x <= 0.98 && 0.89 < y && y <= 0.99) {
					if (Main.perso1.isTourJoueur()) {
						if (leSoldat != null) {
							leSoldat = null;
							touche.restart();
						} else {
							if (Main.perso1.getOr() >= 50) {
								if (((Caserne) Main.grille.getGrille()[batiment.getA()][batiment.getB()][0].isOccuped())
										.getHab() < 10) {
									((Caserne) Main.grille.getGrille()[batiment.getA()][batiment.getB()][0].isOccuped())
											.setHab(((Caserne) Main.grille.getGrille()[batiment.getA()][batiment
													.getB()][0].isOccuped()).getHab() + 1);
									Caserne cas = ((Caserne) Main.grille.getGrille()[batiment.getA()][batiment
											.getB()][0].isOccuped());
									Soldat guerrier = new Guerrier("guerrier", batiment, cas, 1);
									leSoldat = guerrier;
									touche.restart();
								}
							}
						}
					} else {
						if (leSoldat != null) {
							leSoldat = null;
							touche.restart();
						} else {
							if (Main.perso2.getOr() >= 50) {
								if (((Caserne) Main.grille.getGrille()[batiment.getA()][batiment.getB()][0].isOccuped())
										.getHab() < 10) {
									((Caserne) Main.grille.getGrille()[batiment.getA()][batiment.getB()][0].isOccuped())
											.setHab(((Caserne) Main.grille.getGrille()[batiment.getA()][batiment
													.getB()][0].isOccuped()).getHab() + 1);
									Caserne cas = ((Caserne) Main.grille.getGrille()[batiment.getA()][batiment
											.getB()][0].isOccuped());
									Soldat guerrier = new Guerrier("guerrier", batiment, cas, 2);
									Main.perso1.setOr(Main.perso1.getOr() - 50);
									Main.perso1.getEntite().add(guerrier);
									Main.ath.SetImage("Srecru");
									leSoldat = guerrier;
									touche.restart();
								}
							}
						}
					}
				}
				if (0.85 <= x && x <= 0.98 && 0.79 < y && y <= 0.89) {
					touche.restart();
				}
				if (0.85 <= x && x <= 0.98 && 0.69 < y && y <= 0.79) {

					touche.restart();
				}
				if (0.85 <= x && x <= 0.98 && 0.59 < y && y <= 0.69) {
					List<String> l = new ArrayList<String>();
					l.add("soldatLourd");
					l.add("mage");
					l.add("tacticien");
					l.add("assassin");
					l.add("retour");
					Main.onglet.setOnglet(l);
					Main.onglet.setType("soldat2");
					touche.restart();
				}
				if (0.85 <= x && x <= 0.98 && 0.49 < y && y <= 0.59) {
					List<String> l = new ArrayList<String>();
					l.add("ferme");
					l.add("mine");
					l.add("auberge");
					l.add("port");
					l.add("tour");
					Main.onglet.setOnglet(l);
					Main.onglet.setType("build");
					batiment = null;
					touche.restart();
				}
			} else if (Main.onglet.getType() == "soldat2") {
				if (0.85 <= x && x <= 0.98 && 0.89 < y && y <= 0.99) {

					touche.restart();
				}
				if (0.85 <= x && x <= 0.98 && 0.79 < y && y <= 0.89) {
					touche.restart();
				}
				if (0.85 <= x && x <= 0.98 && 0.69 < y && y <= 0.79) {

					touche.restart();
				}
				if (0.85 <= x && x <= 0.98 && 0.59 < y && y <= 0.69) {

					touche.restart();
				}
				if (0.85 <= x && x <= 0.98 && 0.49 < y && y <= 0.59) {
					List<String> l = new ArrayList<String>();
					l.add("guerrier");
					l.add("archer");
					l.add("constructeur");
					l.add("suivant");
					l.add("retour");
					Main.onglet.setOnglet(l);
					Main.onglet.setType("soldat1");
					touche.restart();
				}
			} else if (Main.onglet.getType() == "ferme") {
				if (0.85 <= x && x <= 0.98 && 0.89 < y && y <= 0.99) {

					if (Main.perso1.isTourJoueur()) {
						if (Main.perso1.getOr() >= 50) {
							if (buildChamp) {
								buildChamp = false;
							} else {
								boolean bool = false;
								if (((Ferme) Main.grille.getGrille()[batiment.getA()][batiment.getB()][0].isOccuped())
										.getFermier() < 3) {
									for (Entite hab : Main.perso1.getEntite()) {
										if (hab instanceof Fermier) {
											if (!((Habitant) hab).isWork()) {
												bool = true;
												Main.perso1.setOr(Main.perso1.getOr() - 50);
												touche.restart();
											}
										}
									}
								}
								buildChamp = bool;
							}
						}
					} else {
						if (Main.perso2.getOr() >= 50) {
							if (buildChamp) {
								buildChamp = false;
							} else {
								boolean bool = false;
								if (((Ferme) Main.grille.getGrille()[batiment.getA()][batiment.getB()][0].isOccuped())
										.getFermier() < 3) {
									for (Entite hab : Main.perso2.getEntite()) {
										if (hab instanceof Fermier) {
											if (!((Habitant) hab).isWork()) {
												bool = true;
												Main.perso2.setOr(Main.perso2.getOr() - 50);
												touche.restart();
											}
										}
									}
								}
								buildChamp = bool;
							}
						}
					}
				}
				if (0.85 <= x && x <= 0.98 && 0.79 < y && y <= 0.89) {
					touche.restart();
				}
				if (0.85 <= x && x <= 0.98 && 0.69 < y && y <= 0.79) {

					touche.restart();
				}
				if (0.85 <= x && x <= 0.98 && 0.59 < y && y <= 0.69) {

					touche.restart();
				}
				if (0.85 <= x && x <= 0.98 && 0.49 < y && y <= 0.59) {
					List<String> l = new ArrayList<String>();
					l.add("ferme");
					l.add("mine");
					l.add("auberge");
					l.add("port");
					l.add("tour");
					Main.onglet.setOnglet(l);
					Main.onglet.setType("build");
					batiment = null;
					touche.restart();
				}
			} else if (Main.onglet.getType() == "boat") {
				if (0.85 <= x && x <= 0.98 && 0.89 < y && y <= 0.99) {
					if (mouvBoat != null && mouvBoat.getPassager().size() == 1) {
						mouvSoldat = mouvBoat.getPassager().get(0);
						mouvBoat = null;
					}
					touche.restart();
				}
				if (0.85 <= x && x <= 0.98 && 0.79 < y && y <= 0.89) {
					touche.restart();
				}
				if (0.85 <= x && x <= 0.98 && 0.69 < y && y <= 0.79) {

					touche.restart();
				}
				if (0.85 <= x && x <= 0.98 && 0.59 < y && y <= 0.69) {

					touche.restart();
				}
				if (0.85 <= x && x <= 0.98 && 0.49 < y && y <= 0.59) {
					List<String> l = new ArrayList<String>();
					l.add("ferme");
					l.add("mine");
					l.add("auberge");
					l.add("port");
					l.add("tour");
					Main.onglet.setOnglet(l);
					Main.onglet.setType("build");
					batiment = null;
					touche.restart();
				}
			} else if (Main.onglet.getType() == "port") {
				if (0.85 <= x && x <= 0.98 && 0.89 < y && y <= 0.99) {

					if (Main.perso1.isTourJoueur()) {
						if (Main.perso1.getOr() >= 200) {
							if (buildBoat1) {
								buildBoat1 = false;
							} else {
								boolean bool = false;

								for (Entite hab : Main.perso1.getEntite()) {
									if (hab instanceof Marin) {
										if (!((Habitant) hab).isWork()) {
											bool = true;
											Main.perso1.setOr(Main.perso1.getOr() - 200);

											touche.restart();
										}
									}
								}
								buildBoat1 = bool;
							}
						}
					} else {
						if (Main.perso2.getOr() >= 200) {
							if (buildBoat1) {
								buildBoat1 = false;
							} else {
								boolean bool = false;

								for (Entite hab : Main.perso2.getEntite()) {
									if (hab instanceof Marin) {
										if (!((Habitant) hab).isWork()) {
											bool = true;
											Main.perso2.setOr(Main.perso2.getOr() - 200);
											touche.restart();
										}
									}
								}
								buildBoat1 = bool;
							}
						}
					}
				}
				if (0.85 <= x && x <= 0.98 && 0.79 < y && y <= 0.89) {
					touche.restart();
				}
				if (0.85 <= x && x <= 0.98 && 0.69 < y && y <= 0.79) {

					touche.restart();
				}
				if (0.85 <= x && x <= 0.98 && 0.59 < y && y <= 0.69) {

					touche.restart();
				}
				if (0.85 <= x && x <= 0.98 && 0.49 < y && y <= 0.59) {
					List<String> l = new ArrayList<String>();
					l.add("ferme");
					l.add("mine");
					l.add("auberge");
					l.add("port");
					l.add("tour");
					Main.onglet.setOnglet(l);
					Main.onglet.setType("build");
					batiment = null;
					touche.restart();
				}
			}

			if (buildFerme && !(0.85 <= x && x <= 0.98 && 0.49 <= y && y <= 1)) {

				if (Main.perso1.isTourJoueur()) {
					for (int i = 0; i < Main.grille.getX(); i++) {
						for (int j = 0; j < Main.grille.getY(); j++) {
							if (Main.posGrille[i][j][0].getX() >= x - 0.025
									&& Main.posGrille[i][j][0].getX() <= x + 0.025
									&& Main.posGrille[i][j][0].getY() >= y - 0.025
									&& Main.posGrille[i][j][0].getY() <= y + 0.025) {

								Triplet t = new Triplet(i, j, 0);
								if (Main.grille.getGrille()[i][j][0] != null) {
									if (Main.grille.getGrille()[i][j][0].isBuildable1()) {
										if (0 == Main.grille.getGrille()[i][j][0].getDecord()
												&& 0 == Main.grille.getGrille()[i][j][0].getNiveau()) {
											if (Main.grille.getGrille()[i][j][0].isOccuped() == null) {
												Ferme ferme = new Ferme("ferme", t, 1);
												Main.grille.getGrille()[i][j][0].setOccuped(ferme);
												addTri(ferme);
												Main.perso1.getEntite().add(ferme);
												buildFerme = false;
												touche.restart();
											}
										}
									}
								}
							}
						}
					}
				} else {
					for (int i = 0; i < Main.grille.getX(); i++) {
						for (int j = 0; j < Main.grille.getY(); j++) {
							if (Main.posGrille[i][j][0].getX() >= x - 0.025
									&& Main.posGrille[i][j][0].getX() <= x + 0.025
									&& Main.posGrille[i][j][0].getY() >= y - 0.025
									&& Main.posGrille[i][j][0].getY() <= y + 0.025) {

								Triplet t = new Triplet(i, j, 0);
								if (Main.grille.getGrille()[i][j][0] != null) {
									if (Main.grille.getGrille()[i][j][0].isBuildable2()) {
										if (0 == Main.grille.getGrille()[i][j][0].getDecord()
												&& 0 == Main.grille.getGrille()[i][j][0].getNiveau()) {
											if (Main.grille.getGrille()[i][j][0].isOccuped() == null) {
												Ferme ferme = new Ferme("ferme", t, 2);
												Main.grille.getGrille()[i][j][0].setOccuped(ferme);
												addTri(ferme);
												Main.perso2.getEntite().add(ferme);
												buildFerme = false;
												touche.restart();
											}
										}
									}
								}
							}
						}
					}
				}
			}
			if (buildMine && !(0.85 <= x && x <= 0.98 && 0.49 <= y && y <= 1)) {

				if (Main.perso1.isTourJoueur()) {
					for (int i = 0; i < Main.grille.getX(); i++) {
						for (int j = 0; j < Main.grille.getY(); j++) {
							for (int k = 1; k < Main.grille.getZ(); k++) {
								if (Main.posGrille[i][j][k].getX() >= x - 0.025
										&& Main.posGrille[i][j][k].getX() <= x + 0.025
										&& Main.posGrille[i][j][k].getY() >= y - 0.025
										&& Main.posGrille[i][j][k].getY() <= y + 0.025) {

									Triplet t = new Triplet(i, j, k);
									if (Main.grille.getGrille()[i][j][k] != null
											&& Main.grille.getGrille()[i][j][0].isBuildable1()) {
										if (0 == Main.grille.getGrille()[i][j][k].getDecord()
												&& 0 == Main.grille.getGrille()[i][j][k].getNiveau()
												&& 1 == Main.grille.getGrille()[i][j][0].getNiveau()) {
											if (Main.grille.getGrille()[i][j][0].isOccuped() == null) {
												Mine mine = new Mine("mine", t, 1);
												Main.grille.getGrille()[i][j][0].setOccuped(mine);
												addTri(mine);
												Main.perso1.getEntite().add(mine);
												buildMine = false;
												touche.restart();
											}
										}
									}
								}
							}
						}
					}
				} else {
					for (int i = 0; i < Main.grille.getX(); i++) {
						for (int j = 0; j < Main.grille.getY(); j++) {
							for (int k = 1; k < Main.grille.getZ(); k++) {
								if (Main.posGrille[i][j][k].getX() >= x - 0.025
										&& Main.posGrille[i][j][k].getX() <= x + 0.025
										&& Main.posGrille[i][j][k].getY() >= y - 0.025
										&& Main.posGrille[i][j][k].getY() <= y + 0.025) {

									Triplet t = new Triplet(i, j, k);
									if (Main.grille.getGrille()[i][j][k] != null
											&& Main.grille.getGrille()[i][j][0].isBuildable1()) {
										if (0 == Main.grille.getGrille()[i][j][k].getDecord()
												&& 0 == Main.grille.getGrille()[i][j][k].getNiveau()
												&& 1 == Main.grille.getGrille()[i][j][0].getNiveau()) {
											if (Main.grille.getGrille()[i][j][0].isOccuped() == null) {
												Mine mine = new Mine("mine", t, 2);
												Main.grille.getGrille()[i][j][0].setOccuped(mine);
												addTri(mine);
												Main.perso2.getEntite().add(mine);
												buildMine = false;
												touche.restart();
											}
										}
									}
								}
							}
						}
					}
				}
			}
			if (buildAuberge && !(0.85 <= x && x <= 0.98 && 0.49 <= y && y <= 1)) {

				if (Main.perso1.isTourJoueur()) {
					for (int i = 0; i < Main.grille.getX(); i++) {
						for (int j = 0; j < Main.grille.getY(); j++) {
							if (Main.posGrille[i][j][0].getX() >= x - 0.025
									&& Main.posGrille[i][j][0].getX() <= x + 0.025
									&& Main.posGrille[i][j][0].getY() >= y - 0.025
									&& Main.posGrille[i][j][0].getY() <= y + 0.025) {

								Triplet t = new Triplet(i, j, 0);
								if (Main.grille.getGrille()[i][j][0] != null) {
									if (Main.grille.getGrille()[i][j][0].isBuildable1()) {
										if (0 == Main.grille.getGrille()[i][j][0].getDecord()
												&& 0 == Main.grille.getGrille()[i][j][0].getNiveau()) {
											if (Main.grille.getGrille()[i][j][0].isOccuped() == null) {
												Caserne caserne = new Caserne("auberge", t, 1);
												Main.grille.getGrille()[i][j][0].setOccuped(caserne);
												addTri(caserne);
												Main.perso1.getEntite().add(caserne);
												buildAuberge = false;
												touche.restart();
											}
										}
									}
								}
							}
						}
					}
				} else {
					for (int i = 0; i < Main.grille.getX(); i++) {
						for (int j = 0; j < Main.grille.getY(); j++) {
							if (Main.posGrille[i][j][0].getX() >= x - 0.025
									&& Main.posGrille[i][j][0].getX() <= x + 0.025
									&& Main.posGrille[i][j][0].getY() >= y - 0.025
									&& Main.posGrille[i][j][0].getY() <= y + 0.025) {

								Triplet t = new Triplet(i, j, 0);
								if (Main.grille.getGrille()[i][j][0] != null) {
									if (Main.grille.getGrille()[i][j][0].isBuildable2()) {
										if (0 == Main.grille.getGrille()[i][j][0].getDecord()
												&& 0 == Main.grille.getGrille()[i][j][0].getNiveau()) {
											if (Main.grille.getGrille()[i][j][0].isOccuped() == null) {
												Caserne caserne = new Caserne("auberge", t, 2);
												Main.grille.getGrille()[i][j][0].setOccuped(caserne);
												addTri(caserne);
												Main.perso2.getEntite().add(caserne);
												buildAuberge = false;
												touche.restart();
											}
										}
									}
								}
							}
						}
					}
				}
			}
			if (buildPort && !(0.85 <= x && x <= 0.98 && 0.49 <= y && y <= 1)) {

				if (Main.perso1.isTourJoueur()) {
					for (int i = 0; i < Main.grille.getX(); i++) {
						for (int j = 0; j < Main.grille.getY(); j++) {
							if (Main.posGrille[i][j][0].getX() >= x - 0.025
									&& Main.posGrille[i][j][0].getX() <= x + 0.025
									&& Main.posGrille[i][j][0].getY() >= y - 0.025
									&& Main.posGrille[i][j][0].getY() <= y + 0.025) {

								Triplet t = new Triplet(i, j, 0);
								if (Main.grille.getGrille()[i][j][0] != null) {
									if (Main.grille.getGrille()[i][j][0].isBuildable1()) {
										if (1 == Main.grille.getGrille()[i][j][0].getDecord()
												&& 0 == Main.grille.getGrille()[i][j][0].getNiveau()) {
											if (Main.grille.getGrille()[i][j][0].isOccuped() == null) {
												Porte port = new Porte("port", t, 1);
												Main.grille.getGrille()[i][j][0].setOccuped(port);
												addTri(port);
												Main.perso1.getEntite().add(port);
												buildPort = false;
												touche.restart();
											}
										}
									}
								}
							}
						}
					}
				} else {
					for (int i = 0; i < Main.grille.getX(); i++) {
						for (int j = 0; j < Main.grille.getY(); j++) {
							if (Main.posGrille[i][j][0].getX() >= x - 0.025
									&& Main.posGrille[i][j][0].getX() <= x + 0.025
									&& Main.posGrille[i][j][0].getY() >= y - 0.025
									&& Main.posGrille[i][j][0].getY() <= y + 0.025) {

								Triplet t = new Triplet(i, j, 0);
								if (Main.grille.getGrille()[i][j][0] != null) {
									if (Main.grille.getGrille()[i][j][0].isBuildable2()) {
										if (1 == Main.grille.getGrille()[i][j][0].getDecord()
												&& 0 == Main.grille.getGrille()[i][j][0].getNiveau()) {
											if (Main.grille.getGrille()[i][j][0].isOccuped() == null) {
												Porte port = new Porte("port", t, 2);
												Main.grille.getGrille()[i][j][0].setOccuped(port);
												addTri(port);
												Main.perso2.getEntite().add(port);
												buildPort = false;
												touche.restart();
											}
										}
									}
								}
							}
						}
					}
				}
			}
			if (buildTour && !(0.85 <= x && x <= 0.98 && 0.49 <= y && y <= 1)) {

				if (Main.perso1.isTourJoueur()) {
					for (int i = 0; i < Main.grille.getX(); i++) {
						for (int j = 0; j < Main.grille.getY(); j++) {
							if (Main.posGrille[i][j][0].getX() >= x - 0.025
									&& Main.posGrille[i][j][0].getX() <= x + 0.025
									&& Main.posGrille[i][j][0].getY() >= y - 0.025
									&& Main.posGrille[i][j][0].getY() <= y + 0.025) {

								Triplet t = new Triplet(i, j, 0);
								if (Main.grille.getGrille()[i][j][0] != null) {
									if (Main.grille.getGrille()[i][j][0].isBuildable1()) {
										if (0 == Main.grille.getGrille()[i][j][0].getDecord()
												&& 0 == Main.grille.getGrille()[i][j][0].getNiveau()) {
											if (Main.grille.getGrille()[i][j][0].isOccuped() == null) {
												Tour tour = new Tour("tour", t, 1);
												Main.grille.getGrille()[i][j][0].setOccuped(tour);
												addTri(tour);
												Main.perso1.getEntite().add(tour);
												buildTour = false;
												touche.restart();
											}
										}
									}
								}
							}
						}
					}
				} else {
					for (int i = 0; i < Main.grille.getX(); i++) {
						for (int j = 0; j < Main.grille.getY(); j++) {
							if (Main.posGrille[i][j][0].getX() >= x - 0.025
									&& Main.posGrille[i][j][0].getX() <= x + 0.025
									&& Main.posGrille[i][j][0].getY() >= y - 0.025
									&& Main.posGrille[i][j][0].getY() <= y + 0.025) {

								Triplet t = new Triplet(i, j, 0);
								if (Main.grille.getGrille()[i][j][0] != null) {
									if (Main.grille.getGrille()[i][j][0].isBuildable2()) {
										if (0 == Main.grille.getGrille()[i][j][0].getDecord()
												&& 0 == Main.grille.getGrille()[i][j][0].getNiveau()) {
											if (Main.grille.getGrille()[i][j][0].isOccuped() == null) {
												Tour tour = new Tour("tour", t, 2);
												Main.grille.getGrille()[i][j][0].setOccuped(tour);
												addTri(tour);
												Main.perso2.getEntite().add(tour);
												buildTour = false;
												touche.restart();
											}
										}
									}
								}
							}
						}
					}
				}
			}
			if (buildChamp && !(0.85 <= x && x <= 0.98 && 0.49 <= y && y <= 1)) {

				if (Main.perso1.isTourJoueur()) {
					for (int i = 0; i < Main.grille.getX(); i++) {
						for (int j = 0; j < Main.grille.getY(); j++) {
							if (Main.posGrille[i][j][0].getX() >= x - 0.025
									&& Main.posGrille[i][j][0].getX() <= x + 0.025
									&& Main.posGrille[i][j][0].getY() >= y - 0.025
									&& Main.posGrille[i][j][0].getY() <= y + 0.025) {

								Triplet t = new Triplet(i, j, 0);
								if (Main.grille.getGrille()[i][j][0] != null) {
									if (Main.grille.getGrille()[i][j][0].isFertile1()) {
										if (0 == Main.grille.getGrille()[i][j][0].getDecord()
												&& 0 == Main.grille.getGrille()[i][j][0].getNiveau()) {
											if (Main.grille.getGrille()[i][j][0].isOccuped() == null) {
												boolean stop = false;
												Champ champ = null;
												for (Entite hab : Main.perso1.getEntite()) {
													if (hab instanceof Fermier) {
														if (!((Habitant) hab).isWork() && !stop) {
															((Habitant) hab).setWork(true);

															((Ferme) Main.grille.getGrille()[batiment.getA()][batiment
																	.getB()][0].isOccuped()).setFermier(
																			((Ferme) Main.grille.getGrille()[batiment
																					.getA()][batiment.getB()][0]
																							.isOccuped()).getFermier()
																					+ 1);
															hab.setPosition(
																	new Triplet(batiment.getA(), batiment.getB(), 0));
															champ = new Champ("champ", t, 1);
															((Fermier) hab).setTravaille(champ);
															Main.grille.getGrille()[i][j][0].setOccuped(champ);
															buildChamp = false;
															stop = true;
															touche.restart();
														}
													}
												}
												addTri(champ);
												Main.perso1.getEntite().add(champ);
											}
										}
									}
								}
							}
						}
					}
				} else {
					for (int i = 0; i < Main.grille.getX(); i++) {
						for (int j = 0; j < Main.grille.getY(); j++) {
							if (Main.posGrille[i][j][0].getX() >= x - 0.025
									&& Main.posGrille[i][j][0].getX() <= x + 0.025
									&& Main.posGrille[i][j][0].getY() >= y - 0.025
									&& Main.posGrille[i][j][0].getY() <= y + 0.025) {

								Triplet t = new Triplet(i, j, 0);
								if (Main.grille.getGrille()[i][j][0] != null) {
									if (Main.grille.getGrille()[i][j][0].isFertile2()) {
										if (0 == Main.grille.getGrille()[i][j][0].getDecord()
												&& 0 == Main.grille.getGrille()[i][j][0].getNiveau()) {
											if (Main.grille.getGrille()[i][j][0].isOccuped() == null) {
												boolean stop = false;
												Champ champ = null;
												for (Entite hab : Main.perso2.getEntite()) {
													if (hab instanceof Fermier) {
														if (!((Habitant) hab).isWork() && !stop) {
															((Habitant) hab).setWork(true);
															((Ferme) Main.grille.getGrille()[batiment.getA()][batiment
																	.getB()][0].isOccuped()).setFermier(
																			((Ferme) Main.grille.getGrille()[batiment
																					.getA()][batiment.getB()][0]
																							.isOccuped()).getFermier()
																					+ 1);
															hab.setPosition(
																	new Triplet(batiment.getA(), batiment.getB(), 0));
															champ = new Champ("champ", t, 2);
															((Fermier) hab).setTravaille(champ);
															Main.grille.getGrille()[i][j][0].setOccuped(champ);
															buildChamp = false;
															stop = true;
															touche.restart();
														}
													}
												}
												addTri(champ);
												Main.perso2.getEntite().add(champ);
											}
										}
									}
								}
							}
						}
					}

				}
			}
			if (buildBoat1 && !(0.85 <= x && x <= 0.98 && 0.49 <= y && y <= 1)) {

				if (Main.perso1.isTourJoueur()) {
					for (int i = 0; i < Main.grille.getX(); i++) {
						for (int j = 0; j < Main.grille.getY(); j++) {
							if (Main.posGrille[i][j][0].getX() >= x - 0.025
									&& Main.posGrille[i][j][0].getX() <= x + 0.025
									&& Main.posGrille[i][j][0].getY() >= y - 0.025
									&& Main.posGrille[i][j][0].getY() <= y + 0.025) {

								Triplet t = new Triplet(i, j, 0);
								if (Main.grille.getGrille()[i][j][0] != null) {
									if (Main.grille.getGrille()[i][j][0].isEau1()) {
										if (1 == Main.grille.getGrille()[i][j][0].getDecord()
												&& 0 == Main.grille.getGrille()[i][j][0].getNiveau()) {
											if (Main.grille.getGrille()[i][j][0].isOccuped() == null) {
												boolean stop = false;
												Boat boat = null;
												for (Entite hab : Main.perso1.getEntite()) {
													if (hab instanceof Marin) {
														if (!((Habitant) hab).isWork() && !stop) {
															((Habitant) hab).setWork(true);
															((Porte) Main.grille.getGrille()[batiment.getA()][batiment
																	.getB()][0].isOccuped()).setMarin(
																			((Porte) Main.grille.getGrille()[batiment
																					.getA()][batiment.getB()][0]
																							.isOccuped()).getMarin()
																					+ 1);
															hab.setPosition(
																	new Triplet(batiment.getA(), batiment.getB(), 0));
															boat = new Boat("bateau", t, 1);
															((Marin) hab).setTravaille(boat);
															Main.grille.getGrille()[i][j][0].setOccuped(boat);
															buildBoat1 = false;
															stop = true;
															touche.restart();
														}
													}
												}
												addTri(boat);
												Main.perso1.getEntite().add(boat);
											}
										}
									}
								}
							}
						}
					}
				} else {
					for (int i = 0; i < Main.grille.getX(); i++) {
						for (int j = 0; j < Main.grille.getY(); j++) {
							if (Main.posGrille[i][j][0].getX() >= x - 0.025
									&& Main.posGrille[i][j][0].getX() <= x + 0.025
									&& Main.posGrille[i][j][0].getY() >= y - 0.025
									&& Main.posGrille[i][j][0].getY() <= y + 0.025) {

								Triplet t = new Triplet(i, j, 0);
								if (Main.grille.getGrille()[i][j][0] != null) {
									if (Main.grille.getGrille()[i][j][0].isEau2()) {
										if (1 == Main.grille.getGrille()[i][j][0].getDecord()
												&& 0 == Main.grille.getGrille()[i][j][0].getNiveau()) {
											if (Main.grille.getGrille()[i][j][0].isOccuped() == null) {
												boolean stop = false;
												Boat boat = null;
												for (Entite hab : Main.perso2.getEntite()) {
													if (hab instanceof Marin) {
														if (!((Habitant) hab).isWork() && !stop) {
															((Habitant) hab).setWork(true);
															((Porte) Main.grille.getGrille()[batiment.getA()][batiment
																	.getB()][0].isOccuped()).setMarin(
																			((Porte) Main.grille.getGrille()[batiment
																					.getA()][batiment.getB()][0]
																							.isOccuped()).getMarin()
																					+ 1);
															hab.setPosition(
																	new Triplet(batiment.getA(), batiment.getB(), 0));
															boat = new Boat("bateau", t, 2);
															((Marin) hab).setTravaille(boat);
															Main.grille.getGrille()[i][j][0].setOccuped(boat);
															buildBoat1 = false;
															stop = true;
															touche.restart();
														}
													}
												}
												addTri(boat);
												Main.perso2.getEntite().add(boat);
											}
										}
									}
								}
							}
						}
					}

				}
			}
		}
	}

}
