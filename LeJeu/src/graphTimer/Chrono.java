package graphTimer;


public class Chrono {
	private long debut;
	private long fin; 
	
	public Chrono() {
		debut();
		fin = debut;
	}
	
	public void debut() {
		debut = System.nanoTime();
	}
	
	public void fin() {
		fin = System.nanoTime();
	}
	
	public long time() {
		long div = 1000000000;
		return (fin - debut)/ div ;
	}
}
