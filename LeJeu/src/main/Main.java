package main;


import environment.Entite;
import environment.Grille;
import environment.Perso;
import fenetre.Ath;
import fenetre.Interaction;
import fenetre.Onglet;
import graphTimer.Position;
import graphTimer.StdDraw;
import son.Runner;
import son.Sound;
import son.Stopper;

public class Main {

	public static void playMusique(Sound musique) {
		Runner run = null;
		run = new Runner(musique);
		run.start();
	}
	
	public static void stopMusique(Sound musique) {
		Stopper stop = null;
		stop = new Stopper(musique);
		stop.start();
	}
	
	static double posX =0.5;
	static double posY =0.5;
	
	static double x =0;
	static double y =0;
	static boolean prems = true;
	
	public static int plat = 1;
	
	public static Grille grille;
	
	public static Position[][][] posGrille;
	
	public static Onglet onglet;
	
	public static Ath ath;
	
	public static Interaction interaction = new Interaction();
	
	public static Perso perso1;
	
	public static Perso perso2;
	
	public static void main(String[] args) {
		
		perso1 = new Perso("joueur 1",1);
		perso2 = new Perso("joueur 2",0);
		
		Sound musique = null;
		
		grille = new Grille();
		
		grille.makeGrilleAlea(30, 30, 2, 40, 40, 5);
		posGrille = new Position[grille.getX()][grille.getY()][grille.getZ()];
		
		
		onglet = new Onglet();
		
		ath = new Ath();
		
		StdDraw.setCanvasSize();
		
		StdDraw.enableDoubleBuffering();
		while(true) {
			StdDraw.clear();
			
			
			if (StdDraw.isMousePressed() && prems) {
				x = StdDraw.mouseX();
				y = StdDraw.mouseY();
				prems = false;
			}
			
			if (!StdDraw.isMousePressed()) {
				grille.afficheGrille(plat);
				prems = true;
			}else {
				double tmpX = StdDraw.mouseX();
				double tmpY = StdDraw.mouseY();
				Position p2 = new Position(grille.getP().getX()- (x-tmpX), grille.getP().getY() - (y-tmpY));
				interaction.bougeMap(p2.getX(),p2.getY());
				grille.afficheGrille(plat);
				x = tmpX;
				y = tmpY;
			}
			
			if(StdDraw.hasNextKeyTyped()) {
				interaction.changeMap(StdDraw.nextKeyTyped());
			}
			
			
			if(StdDraw.isMousePressed()) {
				interaction.attaque(interaction.getMouvSoldat(), StdDraw.mouseX(), StdDraw.mouseY());
				interaction.construction(StdDraw.mouseX(), StdDraw.mouseY());
				interaction.action(StdDraw.mouseX(), StdDraw.mouseY());
				interaction.setSoldat(interaction.getLeSoldat(), StdDraw.mouseX(), StdDraw.mouseY());
				
			}
			
			
			interaction.bouge(interaction.getMouvSoldat(), StdDraw.mouseX(), StdDraw.mouseY());
			interaction.bougeBoat(interaction.getMouvBoat(), StdDraw.mouseX(), StdDraw.mouseY());
			
			
			for (Entite ent: perso1.getEntiteAff()) {
			    if(ent != null) ent.affiche(ent.getPos().getX(),ent.getPos().getY());
			}
			for (Entite ent: perso2.getEntiteAff()) {
			    if(ent != null) ent.affiche(ent.getPos().getX(),ent.getPos().getY());
			}
			
			
			onglet.affiche();
			
			ath.affiche();
			ath.voir(StdDraw.mouseX(), StdDraw.mouseY());
			
			StdDraw.show();
			StdDraw.pause(20);
			
			
			try {
				if(musique == null) musique = new Sound("Musics/strat.mp3");
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			if(!musique.isPlaying()) playMusique(musique);
			
			/*
			try {
				if(musique == null) musique = new Sound("Musics/menu.mp3");
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			if(!musique.isPlaying()) playMusique(musique);
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//stopMusique(musique);
			
			try {
				musique = new Sound("Musics/strat.mp3");
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			playMusique(musique);
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			stopMusique(musique);*/
		}
	}

}
