package environment;

import java.util.Random;

import graphTimer.Position;
import graphTimer.Triplet;
import main.Main;

public class Grille {

	private Case[][][] grille;
	private Random rand;
	private int x=100;
	private int y=100;
	private int z=100;
	private Position p;
	
	
	public Grille() {
		grille = null;
		p = new Position(0,0);
	}
	
	public Case[][][] getGrille() {
		return grille;
	}

	public void setGrille(Case[][][] grille) {
		this.grille = grille;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getZ() {
		return z;
	}

	public void setZ(int z) {
		this.z = z;
	}

	public Position getP() {
		return p;
	}

	public void setP(Position p) {
		this.p = p;
	}
	
	public void remouvMouv() {
		for (int i = 0; i < Main.grille.getX(); i++) {
			for (int j = 0; j < Main.grille.getY(); j++) {
				for (int k = 0; k < Main.grille.getZ(); k++) {
					if(Main.grille.getGrille()[i][j][k] != null) Main.grille.getGrille()[i][j][k].setMouv(false);
				}
			}
		}
	}
	
	private int[] div(int a, int b) {
		int x[] = new int[b];
		
		x[0] = a/b;
		
		for(int i=1;i<b;i++) {
			if(b%2!=0) {
				if(i!=(b/2)) {
					x[i]=a/b + x[i-1];
				}else {
					x[i]=(a/b)+(a%b) + x[i-1];
				}
			}else {
				x[i]=a/b + x[i-1];
			}
		}
		
		return x;
	}
	
	
	//faire une grille aléatoire 
	/*
	 			for(int j =0;j<y;j++) {
					for(int k =0;k<z;k++) {
						int lvl = rand.nextInt(2);
						if(!vide) {
							grille[i][j][k] = new Case(lvl, rand.nextInt(4), false);
						}else {
							k=z;
						}
						if(lvl == 0) vide = true;
					}
					vide = false;
				}
	 */


	public Case[][][] makeGrilleAlea(int minX, int minY, int minZ, int maX, int maY, int maZ) {
		
		rand = new Random();
		int maxX = rand.nextInt(maX - minX) + minX;
		int maxY = rand.nextInt(maY - minY) + minY;
		int maxZ = rand.nextInt(maZ - minZ) + minZ;
		
		x = maxX;
		y = maxY;
		z = maxZ;
		
		boolean vide = false;
		
		grille = new Case[x][y][z];
		
		int[] div = div(x,5);
		
		Triplet chateau1 = new Triplet(rand.nextInt(div[1] - div[0]) + div[0], rand.nextInt(y), 0);
		
		Triplet chateau2 = new Triplet(rand.nextInt(div[3] - div[2]) + div[2], rand.nextInt(y), 0);
		
		for(int i =0;i<x;i++) {
			if(i<div[0]) {
				for(int j =0;j<y;j++) {
					for(int k =0;k<z;k++) {
						int lvl = 1;
						int mont = 0;
						if(k >= 6) mont = 3;
						if (z-2 <= k) lvl = rand.nextInt(2);
						if(!vide) {
							Triplet t = new Triplet(i,j,k);
							grille[i][j][k] = new Case(lvl, mont, null, t);
						}else {
							k=z;
						}
						if(lvl == 0) vide = true;
					}
					vide = false;
				}
			}else if(i<div[1]) {
				for(int j =0;j<y;j++) {
					for(int k =0;k<z;k++) {
						Triplet t = new Triplet(i,j,k);
						grille[i][j][k] = new Case(0, 0, null, t);
						
						k=z;
					}
				}
			}else if(i<div[2]) {
				for(int j =0;j<y;j++) {
					for(int k =0;k<z;k++) {
						Triplet t = new Triplet(i,j,k);
						grille[i][j][k] = new Case(0, 1, null, t);
						k=z;
					}
				}
			}else if(i<div[3]) {
				for(int j =0;j<y;j++) {
					for(int k =0;k<z;k++) {
						Triplet t = new Triplet(i,j,k);
						grille[i][j][k] = new Case(0, 0, null, t);
						
						k=z;
					}
				}
			}else if(i<div[4]){
				for(int j =0;j<y;j++) {
					for(int k =0;k<z;k++) {
						int lvl = 1;
						int mont = 0;
						if(k >= 6) mont = 3;
						if (z-2 <= k) lvl = rand.nextInt(2);
						if(!vide) {
							Triplet t = new Triplet(i,j,k);
							grille[i][j][k] = new Case(lvl, mont, null, t);
						}else {
							k=z;
						}
						if(lvl == 0) vide = true;
					}
					vide = false;
				}
			}
		}
		

		
		Chateau chateauA = new Chateau("Chateau", chateau1, 1);
		grille[chateau1.getA()][chateau1.getB()][0].setOccuped(chateauA);
		Main.perso1.getEntite().add(chateauA);
		Main.perso1.getEntiteAff().add(chateauA);
		
		Chateau chateauB = new Chateau("Chateau", chateau2, 2);
		grille[chateau2.getA()][chateau2.getB()][0].setOccuped(chateauB);
		Main.perso2.getEntite().add(chateauB);
		Main.perso2.getEntiteAff().add(chateauB);
		
		return grille;
	}
	
	public void afficheGrille(int plat) {
		if(plat == 0) {
			double valI =0;
			double valJ =0;
			for(int i =0;i<x;i++) {
				valI = 0;
				valJ += 0.031;
				for(int j =0;j<y;j++) {
					valI += 0.05;
						double posI = ((double) x/10) - ((double) i/10);
						double posJ = ((double) y/16) - ((double) j/16);
						double posJ2 = ((double) y*0.032) - ((double) j*0.032);
						
						Position pos = new Position(valI+(posI/2), posJ-posJ2-valJ);
						
						if(grille[i][j][0] != null) {
							grille[i][j][0].dessine(p.getX()+pos.getX(),p.getY()+pos.getY(), grille[i][j][0].isBuildable1(), 0);
						}
						
						Main.posGrille[i][j][0] = pos;
				}
			}
		}else {
			double valI =0;
			double valJ =0;
			for(int i =0;i<x;i++) {
				valI = 0;
				valJ += 0.031;
				for(int j =0;j<y;j++) {
					valI += 0.05;
					for(int k =0;k<z;k++) {
						double posI = ((double) x/10) - ((double) i/10);
						double posJ = ((double) y/15) - ((double) j/15);
						double posK = (double) k/16;
						double posJ2 = ((double) y*0.036) - ((double) j*0.036);
						
						Position pos = new Position(p.getX()+valI+(posI/2), p.getY()+posK+posJ-posJ2-valJ);
						
						if(Main.perso1.isTourJoueur()) {
							if(grille[i][j][k] != null) {
								if(Main.interaction.getLeSoldat() != null) {
									grille[i][j][k].dessine(pos.getX(),pos.getY(), grille[i][j][k].isBuildable1(),  1);
								}else if(Main.interaction.isBuildBoat1()) {
									grille[i][j][k].dessine(pos.getX(),pos.getY(), grille[i][j][k].isEau1(),  4);
								}else if(Main.interaction.isBuildChamp()) {
									grille[i][j][k].dessine(pos.getX(),pos.getY(), grille[i][j][k].isFertile1(),  1);
								}else if(Main.interaction.isBuildFerme()) {
									grille[i][j][k].dessine(pos.getX(),pos.getY(), grille[i][j][k].isBuildable1(),  1);
								}else if(Main.interaction.isBuildMine()) {
									if(grille[i][j][k].getNiveau() == 0 && grille[i][j][0].getNiveau() == 1) {
										grille[i][j][k].dessine(pos.getX(),pos.getY(), grille[i][j][0].isBuildable1(),  2);
									}else {
										grille[i][j][k].dessine(pos.getX(),pos.getY(), false,  2);
									}
								}else if(Main.interaction.isBuildAuberge()) {
									grille[i][j][k].dessine(pos.getX(),pos.getY(), grille[i][j][k].isBuildable1(),  3);
								}else if(Main.interaction.isBuildPort()) {
									grille[i][j][k].dessine(pos.getX(),pos.getY(), grille[i][j][k].isBuildable1(),  4);
								}else if(Main.interaction.isBuildTour()){
									grille[i][j][k].dessine(pos.getX(),pos.getY(), grille[i][j][k].isBuildable1(),  5);
								}else {
									grille[i][j][k].dessine(pos.getX(),pos.getY(), grille[i][j][k].isMouv(),  6);
								}
							}
						}else {
							if(grille[i][j][k] != null) {
								if(Main.interaction.getLeSoldat() != null) {
									grille[i][j][k].dessine(pos.getX(),pos.getY(), grille[i][j][k].isBuildable2(),  1);
								}else if(Main.interaction.isBuildBoat1()) {
									grille[i][j][k].dessine(pos.getX(),pos.getY(), grille[i][j][k].isEau2(),  4);
								}else if(Main.interaction.isBuildChamp()) {
									grille[i][j][k].dessine(pos.getX(),pos.getY(), grille[i][j][k].isFertile2(),  1);
								}else if(Main.interaction.isBuildFerme()) {
									grille[i][j][k].dessine(pos.getX(),pos.getY(), grille[i][j][k].isBuildable2(),  1);
								}else if(Main.interaction.isBuildMine()) {
									if(grille[i][j][k].getNiveau() == 0 && grille[i][j][0].getNiveau() == 1) {
										grille[i][j][k].dessine(pos.getX(),pos.getY(), grille[i][j][0].isBuildable2(),  2);
									}else {
										grille[i][j][k].dessine(pos.getX(),pos.getY(), false,  2);
									}
								}else if(Main.interaction.isBuildAuberge()) {
									grille[i][j][k].dessine(pos.getX(),pos.getY(), grille[i][j][k].isBuildable2(),  3);
								}else if(Main.interaction.isBuildPort()) {
									grille[i][j][k].dessine(pos.getX(),pos.getY(), grille[i][j][k].isBuildable2(),  4);
								}else if(Main.interaction.isBuildTour()){
									grille[i][j][k].dessine(pos.getX(),pos.getY(), grille[i][j][k].isBuildable2(),  5);
								}else {
									grille[i][j][k].dessine(pos.getX(),pos.getY(), grille[i][j][k].isMouv(),  6);
								}
							}
						}
						
						Main.posGrille[i][j][k] = pos;
					}
				}
			}
		}
		
	}
}
