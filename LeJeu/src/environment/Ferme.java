package environment;

import graphTimer.StdDraw;
import graphTimer.Triplet;
import main.Main;

public class Ferme extends Batiment {
	
	private int fermier;
	private int vie =100;

	public Ferme(String nom, Triplet pos, int joueur) {
		super(nom, pos, joueur);
		this.setVie(vie);
		fermier = 0;
		if(Main.perso1.isTourJoueur()) {
			if(!(pos.getA()+1 >= Main.grille.getX()) && !(pos.getB()+1 >= Main.grille.getY())) Main.grille.getGrille()[pos.getA()+1][pos.getB()+1][0].setFertile1(true);
			if(!(pos.getA()-1 < 0) && !(pos.getB()+1 >= Main.grille.getY())) Main.grille.getGrille()[pos.getA()-1][pos.getB()+1][0].setFertile1(true);
			if(!(pos.getA()+1 >= Main.grille.getX()) && !(pos.getB()-1 < 0)) Main.grille.getGrille()[pos.getA()+1][pos.getB()-1][0].setFertile1(true);
			if(!(pos.getA()-1 < 0) && !(pos.getB()-1 < 0)) Main.grille.getGrille()[pos.getA()-1][pos.getB()-1][0].setFertile1(true);
			
			if(!(pos.getA()+1 >= Main.grille.getX())) Main.grille.getGrille()[pos.getA()+1][pos.getB()][0].setFertile1(true);
			if(!(pos.getA()-1 < 0)) Main.grille.getGrille()[pos.getA()-1][pos.getB()][0].setFertile1(true);
			
			if(!(pos.getB()+1 >= Main.grille.getY())) Main.grille.getGrille()[pos.getA()][pos.getB()+1][0].setFertile1(true);
			if(!(pos.getB()-1 < 0)) Main.grille.getGrille()[pos.getA()][pos.getB()-1][0].setFertile1(true);
		}else {
			if(!(pos.getA()+1 >= Main.grille.getX()) && !(pos.getB()+1 >= Main.grille.getY())) Main.grille.getGrille()[pos.getA()+1][pos.getB()+1][0].setFertile2(true);
			if(!(pos.getA()-1 < 0) && !(pos.getB()+1 >= Main.grille.getY())) Main.grille.getGrille()[pos.getA()-1][pos.getB()+1][0].setFertile2(true);
			if(!(pos.getA()+1 >= Main.grille.getX()) && !(pos.getB()-1 < 0)) Main.grille.getGrille()[pos.getA()+1][pos.getB()-1][0].setFertile2(true);
			if(!(pos.getA()-1 < 0) && !(pos.getB()-1 < 0)) Main.grille.getGrille()[pos.getA()-1][pos.getB()-1][0].setFertile2(true);
			
			if(!(pos.getA()+1 >= Main.grille.getX())) Main.grille.getGrille()[pos.getA()+1][pos.getB()][0].setFertile2(true);
			if(!(pos.getA()-1 < 0)) Main.grille.getGrille()[pos.getA()-1][pos.getB()][0].setFertile2(true);
			
			if(!(pos.getB()+1 >= Main.grille.getY())) Main.grille.getGrille()[pos.getA()][pos.getB()+1][0].setFertile2(true);
			if(!(pos.getB()-1 < 0)) Main.grille.getGrille()[pos.getA()][pos.getB()-1][0].setFertile2(true);
		}
	}

	public int getFermier() {
		return fermier;
	}

	public void setFermier(int fermier) {
		this.fermier = fermier;
	}
	
	public int production() {
		int prod =fermier*100;
		return prod;
	}

	@Override
	public void affiche(double x, double y) {
		StdDraw.picture(x, y+0.01, "Image/ferme.png");
	}

	@Override
	public String toString(int x) {
		if(x==1) return fermier + " fermier";
		if(x==2) return vie + " point de vie";
		return "";
	}


}
