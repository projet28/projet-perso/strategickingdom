package environment;

import java.util.ArrayList;
import java.util.List;

public class Perso {
	
	private boolean tourJoueur;
	
	private String nom;
	
	private int or = 1000;
	private int food = 500;
	
	private List<Entite> entiteAff;
	private List<Entite> entite;

	public Perso(String nom, int tour) {
		this.nom= nom;
		if(tour==1) {
			tourJoueur = true;
		}else {
			tourJoueur = false;
		}
		
		entite = new ArrayList<Entite>();
		entiteAff = new ArrayList<Entite>();
	}
	
	public boolean isTourJoueur() {
		return tourJoueur;
	}

	public void setTourJoueur(boolean tourJoueur) {
		this.tourJoueur = tourJoueur;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getOr() {
		return or;
	}

	public void setOr(int or) {
		this.or = or;
	}

	public int getFood() {
		return food;
	}

	public void setFood(int food) {
		this.food = food;
	}

	public List<Entite> getEntite() {
		return entite;
	}

	public void setEntite(List<Entite> entite) {
		this.entite = entite;
	}
	
	public List<Entite> getEntiteAff() {
		return entiteAff;
	}

	public void setEntiteAff(List<Entite> entite) {
		this.entiteAff = entite;
	}

	@Override
	public String toString() {
		return "["+ nom + ", or=" + or + ", food=" + food +"]";
	}
}
