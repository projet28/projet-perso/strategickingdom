package environment;

import java.util.ArrayList;
import java.util.List;

import graphTimer.StdDraw;
import graphTimer.Triplet;
import main.Main;

public class Boat extends Entite{
	
	private int mouv = 4;
	private List<Soldat> passager = new ArrayList<Soldat>();

	public Boat(String nom, Triplet pos, int joueur) {
		super(nom,pos,joueur);
		this.setVie(20);
	}

	public int getMouv() {
		return mouv;
	}

	public void setMouv(int mouv) {
		this.mouv = mouv;
	}

	public List<Soldat> getPassager() {
		return passager;
	}

	public void setPassager(List<Soldat> passager) {
		this.passager = passager;
	}
	
	public void retournMouv() {
		this.setMouv(4);
	}

	public void destruction() {
		if(Main.perso1.isTourJoueur()) {
			int i = 0;
			int eff = 0;
			for(Entite ent: Main.perso1.getEntite()) {
				if(ent instanceof Boat && this.getPosition() == ent.getPosition()) {
					eff = i;
				}
				i++;
			}
			Main.perso1.getEntite().remove(eff);
		}else {
			int i = 0;
			int eff = 0;
			for(Entite ent: Main.perso2.getEntite()) {
				if(ent instanceof Boat && this.getPosition() == ent.getPosition()) {
					eff = i;
				}
				i++;
			}
			Main.perso2.getEntite().remove(eff);
		}
		
		Main.grille.getGrille()[this.getPosition().getA()][this.getPosition().getB()][this.getPosition().getC()].setOccuped(null);
	}

	@Override
	public void affiche(double x, double y) {
		StdDraw.picture(x, y+0.01, "Image/bateauPetit.png");
	}
	
	@Override
	public String toString(int x) {
		if(x==1) return this.getVie() + " point de vie";
		if(x==2) return mouv + "point de mouvement";
		return "";
	}
}
