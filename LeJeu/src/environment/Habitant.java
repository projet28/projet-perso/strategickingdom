package environment;

import graphTimer.Triplet;

public abstract class Habitant extends Entite{
	
	private boolean work;
	private Caserne embauche;
	
	public Habitant(String name, Triplet t, Caserne c, int joueur) {
		super(name, t, joueur);
		this.embauche = c;
		work = false;
	}

	public boolean isWork() {
		return work;
	}

	public void setWork(boolean work) {
		this.work = work;
	}

	public Caserne getEmbauche() {
		return embauche;
	}

	public void setEmbauche(Caserne embauche) {
		this.embauche = embauche;
	}

	@Override
	public abstract void affiche(double x, double y) ;

	public abstract String toString(int x) ;

}
