package environment;

import graphTimer.StdDraw;
import graphTimer.Triplet;

public class Caserne extends Batiment {
	
	private int vie = 100;
	private int hab;

	public Caserne(String nom, Triplet pos, int joueur) {
		super(nom, pos, joueur);
		hab =0;
		this.setVie(vie);
	}

	public int getHab() {
		return hab;
	}

	public void setHab(int hab) {
		this.hab = hab;
	}

	@Override
	public void affiche(double x, double y) {
		StdDraw.picture(x, y+0.01, "Image/auberge.png");
	}

	@Override
	public String toString(int x) {
		if(x==1) return hab + " habitant";
		if(x==2) return vie + " point de vie";
		return "";
	}

}
