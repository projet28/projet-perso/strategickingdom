package environment;

import graphTimer.StdDraw;
import graphTimer.Triplet;

public class Guerrier extends Soldat{
	

	public Guerrier(String nom, Triplet t, Caserne c, int joueur) {
		super(nom, t, c, joueur);
		this.setVie(10);
		this.setMouv(2);
		this.setAtt(5);
	}
	
	@Override
	public void retournMouv() {
		this.setMouv(2);
	}
	

	@Override
	public void affiche(double x, double y) {
		if(this.isNavig() == null) StdDraw.picture(x, y+0.04, "Image/guerrier.png");
	}


	@Override
	public String toString(int x) {
		if(this.isNavig() == null) {
			if(x==1) return this.getVie() + " point de vie ";
			if(x==2) return this.getMouv() + " point de mouvement";
		}
		return null;
	}

}
