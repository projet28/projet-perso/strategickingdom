package environment;

import graphTimer.Triplet;

public abstract class Batiment extends Entite{

	
	Batiment(String nom, Triplet pos, int joueur){
		super(nom,pos, joueur);
	}
	
	public abstract void affiche(double x, double y);

	abstract public String toString(int x);
	
	
}
