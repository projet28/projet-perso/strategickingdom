package environment;

import graphTimer.Triplet;

public class Fermier extends Habitant{
	
	private Champ travaille;

	public Fermier(String nom, Triplet t, Caserne c, Champ ch, int joueur){
		super(nom, t, c, joueur);
		this.travaille = ch;
		this.setVie(1);
	}

	public Champ getTravaille() {
		return travaille;
	}

	public void setTravaille(Champ travaille) {
		this.travaille = travaille;
	}

	@Override
	public void affiche(double x, double y) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String toString(int x) {
		return "";
	}
}
