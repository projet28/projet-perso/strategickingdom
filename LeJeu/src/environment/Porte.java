package environment;

import graphTimer.StdDraw;
import graphTimer.Triplet;
import main.Main;

public class Porte extends Batiment {
	
	private int vie = 100;
	private int marin = 0;

	public Porte(String nom, Triplet pos, int joueur) {
		super(nom, pos, joueur);
		this.setVie(vie);
		
		if(Main.perso1.isTourJoueur()) {
			if(!(pos.getA()+1 >= Main.grille.getX()) && !(pos.getB()+1 >= Main.grille.getY())) Main.grille.getGrille()[pos.getA()+1][pos.getB()+1][0].setEau1(true);
			if(!(pos.getA()-1 < 0) && !(pos.getB()+1 >= Main.grille.getY())) Main.grille.getGrille()[pos.getA()-1][pos.getB()+1][0].setEau1(true);
			if(!(pos.getA()+1 >= Main.grille.getX()) && !(pos.getB()-1 < 0)) Main.grille.getGrille()[pos.getA()+1][pos.getB()-1][0].setEau1(true);
			if(!(pos.getA()-1 < 0) && !(pos.getB()-1 < 0)) Main.grille.getGrille()[pos.getA()-1][pos.getB()-1][0].setEau1(true);
			
			if(!(pos.getA()+1 >= Main.grille.getX())) Main.grille.getGrille()[pos.getA()+1][pos.getB()][0].setEau1(true);
			if(!(pos.getA()-1 < 0)) Main.grille.getGrille()[pos.getA()-1][pos.getB()][0].setEau1(true);
			
			if(!(pos.getB()+1 >= Main.grille.getY())) Main.grille.getGrille()[pos.getA()][pos.getB()+1][0].setEau1(true);
			if(!(pos.getB()-1 < 0)) Main.grille.getGrille()[pos.getA()][pos.getB()-1][0].setEau1(true);
		}else {
			if(!(pos.getA()+1 >= Main.grille.getX()) && !(pos.getB()+1 >= Main.grille.getY())) Main.grille.getGrille()[pos.getA()+1][pos.getB()+1][0].setEau2(true);
			if(!(pos.getA()-1 < 0) && !(pos.getB()+1 >= Main.grille.getY())) Main.grille.getGrille()[pos.getA()-1][pos.getB()+1][0].setEau2(true);
			if(!(pos.getA()+1 >= Main.grille.getX()) && !(pos.getB()-1 < 0)) Main.grille.getGrille()[pos.getA()+1][pos.getB()-1][0].setEau2(true);
			if(!(pos.getA()-1 < 0) && !(pos.getB()-1 < 0)) Main.grille.getGrille()[pos.getA()-1][pos.getB()-1][0].setEau2(true);
			
			if(!(pos.getA()+1 >= Main.grille.getX())) Main.grille.getGrille()[pos.getA()+1][pos.getB()][0].setEau2(true);
			if(!(pos.getA()-1 < 0)) Main.grille.getGrille()[pos.getA()-1][pos.getB()][0].setEau2(true);
			
			if(!(pos.getB()+1 >= Main.grille.getY())) Main.grille.getGrille()[pos.getA()][pos.getB()+1][0].setEau2(true);
			if(!(pos.getB()-1 < 0)) Main.grille.getGrille()[pos.getA()][pos.getB()-1][0].setEau2(true);
		}
	}

	public int getMarin() {
		return marin;
	}

	public void setMarin(int marin) {
		this.marin = marin;
	}

	@Override
	public void affiche(double x, double y) {
		StdDraw.picture(x, y+0.01, "Image/port.png");
	}

	@Override
	public String toString(int x) {
		if(x==1) return marin + " marin";
		if(x==2) return vie + " point de vie";
		return "";
	}


}
