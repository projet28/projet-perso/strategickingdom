package environment;

import graphTimer.Triplet;

public class Marin extends Habitant{
	
	private Boat travaille;

	public Marin(String nom, Triplet t, Caserne c, Boat b, int joueur) {
		super(nom, t, c, joueur);
		this.travaille = b;
		this.setVie(1);
	}

	public Boat getTravaille() {
		return travaille;
	}

	public void setTravaille(Boat travaille) {
		this.travaille = travaille;
	}

	@Override
	public void affiche(double x, double y) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String toString(int x) {
		return "";
	}
}
