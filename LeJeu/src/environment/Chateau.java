package environment;

import graphTimer.StdDraw;
import graphTimer.Triplet;

public class Chateau extends Batiment{
	

	public Chateau(String nom, Triplet pos, int joueur) {
		super(nom, pos, joueur);
		this.setVie(1000);
	}

	@Override
	public void affiche(double x, double y) {
		StdDraw.picture(x, y+0.01, "Image/chateau.png");
	}

	@Override
	public String toString(int x) {
		if(x!=2) return this.getVie() + " point de vie";
		return "";
	}

}
