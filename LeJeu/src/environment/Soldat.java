package environment;

import graphTimer.Triplet;

public abstract class Soldat extends Habitant{
	
	private int mouv;
	private int att;
	private Boat navig = null;

	public Soldat(String nom, Triplet t, Caserne c, int joueur) {
		super(nom, t, c, joueur);
	}
	
	
	public int getAtt() {
		return att;
	}


	public void setAtt(int att) {
		this.att = att;
	}


	public int getMouv() {
		return mouv;
	}


	public void setMouv(int mouv) {
		this.mouv = mouv;
	}
	
	public Boat isNavig() {
		return navig;
	}


	public void setNavig(Boat navig) {
		this.navig = navig;
	}


	public abstract void retournMouv();

	public abstract void affiche(double x, double y);

}
