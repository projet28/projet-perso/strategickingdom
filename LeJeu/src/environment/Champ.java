package environment;

import graphTimer.StdDraw;
import graphTimer.Triplet;
import main.Main;

public class Champ extends Batiment{

	public Champ(String nom, Triplet pos, int joueur) {
		super(nom, pos, joueur);
		this.setVie(1);
	}
	
	public void destruction() {
		if(Main.perso1.isTourJoueur()) {
			int i = 0;
			int eff = 0;
			for(Entite ent: Main.perso1.getEntite()) {
				if(ent instanceof Champ && this.getPosition() == ent.getPosition()) {
					eff = i;
				}
				i++;
			}
			Main.perso1.getEntite().remove(eff);
		}else {
			int i = 0;
			int eff = 0;
			for(Entite ent: Main.perso2.getEntite()) {
				if(ent instanceof Champ && this.getPosition() == ent.getPosition()) {
					eff = i;
				}
				i++;
			}
			Main.perso2.getEntite().remove(eff);
		}
		
		Main.grille.getGrille()[this.getPosition().getA()][this.getPosition().getB()][this.getPosition().getC()].setOccuped(null);
	}

	@Override
	public void affiche(double x, double y) {
		StdDraw.picture(x, y+0.02, "Image/champ1.png");
	}

	@Override
	public String toString(int x) {
		return null;
	}

}
