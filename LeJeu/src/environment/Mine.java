package environment;

import graphTimer.StdDraw;
import graphTimer.Triplet;
import main.Main;

public class Mine extends Batiment {
	
	private int mineur;
	private int vie=100;

	public Mine(String nom, Triplet pos, int joueur) {
		super(nom, pos, joueur);
		this.setVie(vie);
		mineur = 0;
		if(Main.perso1.isTourJoueur()) {
			for(Entite hab : Main.perso1.getEntite()) {
				if(mineur < 5) {
					if(hab instanceof Mineur) {
						if(!((Habitant) hab).isWork()) {
							mineur ++;
							((Habitant) hab).setWork(true);
						}
					}
				}
			}
		}else {
			for(Entite hab : Main.perso2.getEntite()) {
				if(mineur < 5) {
					if(hab instanceof Mineur) {
						if(!((Habitant) hab).isWork()) {
							mineur ++;
							((Habitant) hab).setWork(true);
						}
					}
				}
			}
		}
	}

	public int getMineur() {
		return mineur;
	}

	public void setMineur(int mineur) {
		this.mineur = mineur;
	}
	
	public int production() {
		int prod =mineur*100;
		return prod;
	}

	@Override
	public void affiche(double x, double y) {
		StdDraw.picture(x, y+0.04, "Image/mine.png");
	}

	@Override
	public String toString(int x) {
		if(x==1) return mineur + "mineur";
		if(x==2) return vie + " point de vie";
		return "";
	}


}
