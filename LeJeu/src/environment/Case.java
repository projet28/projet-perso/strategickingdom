package environment;

import graphTimer.StdDraw;
import graphTimer.Triplet;
import main.Main;

public class Case{

	private int niveau; //0 plat 1 élevé 2 tunnel 3 vide
	private int decord; //0 terre 1 mer 2 plage 3 neige
	private Entite occuped;
	private Triplet pos;
	
	private boolean buildable1;
	private boolean fertile1;
	private boolean eau1;
	
	private boolean buildable2;
	private boolean fertile2;
	private boolean eau2;
	
	private boolean mouv;
	
	
	public Case(int niveau, int decord, Entite occup, Triplet pos) {
		this.niveau = niveau;
		this.decord = decord;
		this.occuped = occup;
		this.pos = pos;
		this.buildable1 = false;
		this.buildable2 = false;
	}
	public int getNiveau() {
		return niveau;
	}

	public void setNiveau(int niveau) {
		this.niveau = niveau;
	}

	public int getDecord() {
		return decord;
	}

	public void setDecord(int decord) {
		this.decord = decord;
	}

	public Entite isOccuped() {
		return occuped;
	}

	public void setOccuped(Entite occuped) {
		if(occuped == null) {
			this.occuped = null;
		}else {
			this.occuped = occuped;
			if(occuped.getJoueur() == 1) {
				if(Main.grille != null) {
					Main.grille.getGrille()[pos.getA()][pos.getB()][0].setBuildable1(true);
					occuped.getVisit().add(Main.grille.getGrille()[pos.getA()][pos.getB()][0]);
					if(!(pos.getA()+1 >= Main.grille.getX()) && !(pos.getB()+1 >= Main.grille.getY())) {
						Main.grille.getGrille()[pos.getA()+1][pos.getB()+1][0].setBuildable1(true);
						occuped.getVisit().add(Main.grille.getGrille()[pos.getA()+1][pos.getB()+1][0]);
					}
					if(!(pos.getA()-1 < 0) && !(pos.getB()+1 >= Main.grille.getY())) {
						Main.grille.getGrille()[pos.getA()-1][pos.getB()+1][0].setBuildable1(true);
						occuped.getVisit().add(Main.grille.getGrille()[pos.getA()-1][pos.getB()+1][0]);
					}
					if(!(pos.getA()+1 >= Main.grille.getX()) && !(pos.getB()-1 < 0)) {
						Main.grille.getGrille()[pos.getA()+1][pos.getB()-1][0].setBuildable1(true);
						occuped.getVisit().add(Main.grille.getGrille()[pos.getA()+1][pos.getB()-1][0]);
					}
					if(!(pos.getA()-1 < 0) && !(pos.getB()-1 < 0)) {
						Main.grille.getGrille()[pos.getA()-1][pos.getB()-1][0].setBuildable1(true);
						occuped.getVisit().add(Main.grille.getGrille()[pos.getA()-1][pos.getB()-1][0]);
					}
					
					if(!(pos.getB()+1 >= Main.grille.getY())) {
						Main.grille.getGrille()[pos.getA()][pos.getB()+1][0].setBuildable1(true);
						occuped.getVisit().add(Main.grille.getGrille()[pos.getA()][pos.getB()+1][0]);
					}
					if(!(pos.getA()+1 >= Main.grille.getX())) {
						Main.grille.getGrille()[pos.getA()+1][pos.getB()][0].setBuildable1(true);
						occuped.getVisit().add(Main.grille.getGrille()[pos.getA()+1][pos.getB()][0]);
					}
					
					if(!(pos.getA()-1 < 0)) {
						Main.grille.getGrille()[pos.getA()-1][pos.getB()][0].setBuildable1(true);
						occuped.getVisit().add(Main.grille.getGrille()[pos.getA()-1][pos.getB()][0]);
					}
					if(!(pos.getB()-1 < 0)) {
						Main.grille.getGrille()[pos.getA()][pos.getB()-1][0].setBuildable1(true);
						occuped.getVisit().add(Main.grille.getGrille()[pos.getA()][pos.getB()-1][0]);
					}
				}
			}else {
				if(Main.grille != null) {
					Main.grille.getGrille()[pos.getA()][pos.getB()][0].setBuildable2(true);
					occuped.getVisit().add(Main.grille.getGrille()[pos.getA()][pos.getB()][0]);
					if(!(pos.getA()+1 >= Main.grille.getX()) && !(pos.getB()+1 >= Main.grille.getY())) {
						Main.grille.getGrille()[pos.getA()+1][pos.getB()+1][0].setBuildable2(true);
						occuped.getVisit().add(Main.grille.getGrille()[pos.getA()+1][pos.getB()+1][0]);
					}
					if(!(pos.getA()-1 < 0) && !(pos.getB()+1 >= Main.grille.getY())) {
						Main.grille.getGrille()[pos.getA()-1][pos.getB()+1][0].setBuildable2(true);
						occuped.getVisit().add(Main.grille.getGrille()[pos.getA()-1][pos.getB()+1][0]);
					}
					if(!(pos.getA()+1 >= Main.grille.getX()) && !(pos.getB()-1 < 0)) {
						Main.grille.getGrille()[pos.getA()+1][pos.getB()-1][0].setBuildable2(true);
						occuped.getVisit().add(Main.grille.getGrille()[pos.getA()+1][pos.getB()-1][0]);
					}
					if(!(pos.getA()-1 < 0) && !(pos.getB()-1 < 0)) {
						Main.grille.getGrille()[pos.getA()-1][pos.getB()-1][0].setBuildable2(true);
						occuped.getVisit().add(Main.grille.getGrille()[pos.getA()-1][pos.getB()-1][0]);
					}
					
					if(!(pos.getB()+1 >= Main.grille.getY())) {
						Main.grille.getGrille()[pos.getA()][pos.getB()+1][0].setBuildable2(true);
						occuped.getVisit().add(Main.grille.getGrille()[pos.getA()][pos.getB()+1][0]);
					}
					if(!(pos.getA()+1 >= Main.grille.getX())) {
						Main.grille.getGrille()[pos.getA()+1][pos.getB()][0].setBuildable2(true);
						occuped.getVisit().add(Main.grille.getGrille()[pos.getA()+1][pos.getB()][0]);
					}
					
					if(!(pos.getA()-1 < 0)) {
						Main.grille.getGrille()[pos.getA()-1][pos.getB()][0].setBuildable2(true);
						occuped.getVisit().add(Main.grille.getGrille()[pos.getA()-1][pos.getB()][0]);
					}
					if(!(pos.getB()-1 < 0)) {
						Main.grille.getGrille()[pos.getA()][pos.getB()-1][0].setBuildable2(true);
						occuped.getVisit().add(Main.grille.getGrille()[pos.getA()][pos.getB()-1][0]);
					}
				}
			}
		}
	}
	
	public boolean isFertile1() {
		return fertile1;
	}
	public void setFertile1(boolean fertile) {
		this.fertile1 = fertile;
	}
	
	public boolean isEau1() {
		return eau1;
	}
	
	public void setEau1(boolean eau) {
		this.eau1 = eau;
	}
	
	public boolean isFertile2() {
		return fertile2;
	}
	public void setFertile2(boolean fertile) {
		this.fertile2 = fertile;
	}
	
	public boolean isEau2() {
		return eau2;
	}
	
	public void setEau2(boolean eau) {
		this.eau2 = eau;
	}
	
	public Triplet getPos() {
		return pos;
	}
	
	public void setPos(Triplet pos) {
		this.pos = pos;
	}
	
	public boolean isBuildable1() {
		return buildable1;
	}
	
	public void setBuildable1(boolean buildable) {
		this.buildable1 = buildable;
	}
	
	public boolean isBuildable2() {
		return buildable2;
	}
	
	public void setBuildable2(boolean buildable) {
		this.buildable2 = buildable;
	}
	
	public boolean isMouv() {
		return mouv;
	}
	
	public void setMouv(boolean mouv) {
		this.mouv = mouv;
	}
	
	public void dessine(double x, double y, boolean build, int aff) {
		if(this.niveau!=3) {
			if(this.niveau==1) {
				if(this.decord==0) StdDraw.picture(x, y, "Image/marron.png");
				if(this.decord==3) StdDraw.picture(x, y, "Image/blanc.png");
			}
			if(this.niveau==0) {
				if(this.decord==0) {
					if((aff==1 || aff==3 || aff==2 || aff==5 || aff==6) && build) {
						StdDraw.picture(x, y, "Image/vertR.png");
					}else {
						StdDraw.picture(x, y, "Image/vert.png");
					}
				}
				if(this.decord==1) {
					if((aff==4 || aff==6 )&& build) {
						StdDraw.picture(x, y, "Image/bleueR.png");
					}else {
						StdDraw.picture(x, y, "Image/bleue.png");
					}
				}
				if(this.decord==2) StdDraw.picture(x, y, "Image/beige.png");
			}
		}
	}

	@Override
	public String toString() {
		return "Zone [niveau=" + niveau + ", decord=" + decord + ", occuped=" + occuped + "]";
	}
}
