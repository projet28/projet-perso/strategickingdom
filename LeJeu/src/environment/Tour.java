package environment;

import graphTimer.StdDraw;
import graphTimer.Triplet;

public class Tour extends Batiment{
	
	private int vie=100;
	
	public Tour(String nom, Triplet pos, int joueur) {
		super(nom, pos, joueur);
		this.setVie(vie);
	}

	@Override
	public void affiche(double x, double y) {
		StdDraw.picture(x, y+0.015, "Image/tour.png");
	}

	@Override
	public String toString(int x) {
		if(x!=2) return vie + " point de vie";
		return "";
	}
}
