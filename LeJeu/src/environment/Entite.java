package environment;

import java.util.ArrayList;
import java.util.List;

import graphTimer.Position;
import graphTimer.Triplet;
import main.Main;

public abstract class Entite {
	
	private String nom;
	private Triplet pos;
	private int joueur;
	private int vie;
	private List<Case> visit = new ArrayList<Case>(); 
	
	Entite(String nom, Triplet pos, int joueur){
		this.nom = nom;
		this.pos = pos;
		this.joueur = joueur;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public Triplet getPosition() {
		return pos;
	}

	public void setPosition(Triplet pos) {
		this.pos = pos;
	}

	public Position getPos() {
		return Main.posGrille[pos.getA()][pos.getB()][pos.getC()];
	}

	public void setPos(Position p) {
		Main.posGrille[pos.getA()][pos.getB()][pos.getC()] = p;
	}

	public int getJoueur() {
		return joueur;
	}

	public void setJoueur(int joueur) {
		this.joueur = joueur;
	}

	public int getVie() {
		return vie;
	}

	public void setVie(int vie) {
		this.vie = vie;
	}

	public List<Case> getVisit() {
		return visit;
	}

	public void setVisit(List<Case> visit) {
		this.visit = visit;
	}

	public abstract void affiche(double x, double y);

	abstract public String toString(int x);
	
	
}
