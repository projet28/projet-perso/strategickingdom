package environment;

import graphTimer.Triplet;

public class Mineur extends Habitant{

	public Mineur(String nom, Triplet t, Caserne c, int joueur){
		super(nom, t, c, joueur);
		this.setVie(1);
	}

	@Override
	public void affiche(double x, double y) {
	}

	@Override
	public String toString(int x) {
		return "";
	}
}
